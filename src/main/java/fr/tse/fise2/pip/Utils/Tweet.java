package fr.tse.fise2.pip.Utils;

public class Tweet implements Comparable<Tweet> {
	private String screen_name;
	private String tweetText;
	private Double favoriteCount;
	private Double retweetCount;

	/**
	 * @param screen_name   nom d'utilisateur de la personne ayant tweete
	 * @param tweetText     tweet au format extend 240 caracteres
	 * @param favoriteCount nombre de favoris sur le tweet
	 * @param retweetCount  nombre de retweet sur le tweet
	 */
	public Tweet(String screen_name, String tweetText, Double favoriteCount, Double retweetCount) {
		super();
		this.screen_name = screen_name;
		this.tweetText = tweetText;
		this.favoriteCount = favoriteCount;
		this.retweetCount = retweetCount;
	}

	@Override
	public int compareTo(Tweet t) {
		double totalThis = this.retweetCount * 1.5 + this.favoriteCount;
		double totalOther = t.getRetweetCount() * 1.5 + t.getFavoriteCount();
		int countComp = -Double.compare(totalThis, totalOther);
		return countComp;
	}

	public String getscreen_name() {
		return screen_name;
	}

	public void setscreen_name(String screen_name) {
		this.screen_name = screen_name;
	}

	public String getTweetText() {
		return tweetText;
	}

	public void setTweetText(String tweetText) {
		this.tweetText = tweetText;
	}

	public Double getFavoriteCount() {
		return favoriteCount;
	}

	public void setFavoriteCount(Double favoriteCount) {
		this.favoriteCount = favoriteCount;
	}

	public Double getRetweetCount() {
		return retweetCount;
	}

	public void setRetweetCount(Double retweetCount) {
		this.retweetCount = retweetCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((favoriteCount == null) ? 0 : favoriteCount.hashCode());
		result = prime * result + ((screen_name == null) ? 0 : screen_name.hashCode());
		result = prime * result + ((retweetCount == null) ? 0 : retweetCount.hashCode());
		result = prime * result + ((tweetText == null) ? 0 : tweetText.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tweet other = (Tweet) obj;
		if (favoriteCount == null) {
			if (other.favoriteCount != null)
				return false;
		} else if (!favoriteCount.equals(other.favoriteCount))
			return false;
		if (screen_name == null) {
			if (other.screen_name != null)
				return false;
		} else if (!screen_name.equals(other.screen_name))
			return false;
		if (retweetCount == null) {
			if (other.retweetCount != null)
				return false;
		} else if (!retweetCount.equals(other.retweetCount))
			return false;
		if (tweetText == null) {
			if (other.tweetText != null)
				return false;
		} else if (!tweetText.equals(other.tweetText))
			return false;
		return true;
	}
}
