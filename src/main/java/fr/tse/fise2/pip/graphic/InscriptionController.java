package fr.tse.fise2.pip.graphic;

import fr.tse.fise2.pip.Database.Database;
import fr.tse.fise2.pip.Exceptions.IncorrectPasswordException;
import fr.tse.fise2.pip.Exceptions.IncorrectSpaceInUsername;
import fr.tse.fise2.pip.Exceptions.UserAlreadyRegisteredException;
import fr.tse.fise2.pip.Exceptions.UserNotExistException;
import fr.tse.fise2.pip.Utils.Session;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * Controlleur qui fait le lien avec l'interface graphique pour l'inscription
 * d'un nouvelle utilisateur.
 * 
 * @author Queffelec Matthieu
 */
public class InscriptionController {

	@FXML
	private TextField loginField;

	@FXML
	private PasswordField passwordField;

	@FXML
	private Button creatButton;

	@FXML
	private PasswordField passwordBisField;

	private Session session;
	private MainController mainController;

	public InscriptionController(Session session, MainController mainController) {
		this.session = session;
		this.mainController = mainController;
	}

	/**
	 * Methode qui verifie que l'utilisateur a bien remplie tous les champs. que les
	 * mots de passes sont bien les meme et que le login n'est pas deja utiliser
	 * avant d'envoyer la requete a la base de donner et de connecter l'utilisateur.
	 * 
	 * @param event
	 */
	@FXML
	void createAccount(ActionEvent event) {
		if (loginField.getText().equals("") || passwordField.getText().equals("")
				|| passwordBisField.getText().equals("")) {
			Alert alert = AlertCreator.createError("Un champ est vide");
			alert.showAndWait();
		} else {
			if (!passwordField.getText().equals(passwordBisField.getText())) {
				Alert alert = AlertCreator.createError("Les mots de passe sont differents");
				alert.showAndWait();
			} else {
				try {
					Database database = session.getDatabase();
					boolean pass;
					pass = database.registerAccount(loginField.getText(), passwordField.getText(), new String("test"));
					if (pass) {
						Alert alert = AlertCreator.createInfo("Le nouveau utilisateur a ete cree !");
						database.loginAccount(loginField.getText(), passwordField.getText(), this.session);
						mainController.setHomeScreen();
						alert.showAndWait();
					} else {
						Alert alert = AlertCreator.createError("L'utilisateur n'a pas pu etre cree.");
						alert.showAndWait();
					}
				} catch (UserAlreadyRegisteredException e) {
					Alert alert = AlertCreator.createError("Le login est deja utilise par un autre utilisateur");
					alert.showAndWait();
				} catch (UserNotExistException e) {
					e.printStackTrace();
					Alert alert = AlertCreator.createError("L'utilisateur n'existe pas.");
					alert.showAndWait();
				} catch (IncorrectPasswordException e) {
					e.printStackTrace();
					Alert alert = AlertCreator.createError("Le mot de passe est incorrect");
					alert.showAndWait();
				} catch (IncorrectSpaceInUsername incorrectSpaceInUsername) {
					incorrectSpaceInUsername.printStackTrace();
					Alert alert = AlertCreator.createError("Le login comporte des espaces, veuillez les enlever");
					alert.showAndWait();
				}
			}
		}

	}

}