package fr.tse.fise2.pip.graphic;

import com.jfoenix.controls.JFXPasswordField;
import fr.tse.fise2.pip.Exceptions.IncorrectPasswordException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;

/**
 * Classe qui fait le lien avec l'affichage de la page Mon Compte de
 * l'utilisateur et qui permet de changer le mot de passe de celui-ci.
 */
public class AccountController {

	private MainController mainController;

	@FXML
	private JFXPasswordField oldPassword;

	@FXML
	private JFXPasswordField newPassword;

	@FXML
	private JFXPasswordField newPassword2;

	@FXML
	private Label loginLabel;

	public AccountController(MainController mainController) {
		this.mainController = mainController;
	}

	/**
	 * Methode qui met a jour le label avec le nom de l'utilisateur
	 */
	public void setPanel() {
		loginLabel.setText(mainController.getSession().getUsername());
	}

	/**
	 * Methode qui met a jour la database avec le nouveau mot de passe de
	 * l'utilisateur si celui-ci a correctement rentre les champs
	 * 
	 * @param event
	 */
	@FXML
	void changeClick(ActionEvent event) {
		if (oldPassword.getText().equals("") || newPassword.getText().equals("") || oldPassword.getText().equals("")) {
			Alert alert = AlertCreator.createError("Un champ est vide");
			alert.showAndWait();
		} else {
			if (!newPassword.getText().equals(newPassword2.getText())) {
				Alert alert = AlertCreator.createError("Les mots de passe sont differents");
				alert.showAndWait();
			} else {
				try {
					mainController.getSession().getDatabase().changePassword(oldPassword.getText(),
							newPassword.getText(), mainController.getSession());
					Alert alert = AlertCreator.createInfo("Mot de passe change avec succes");
					alert.showAndWait();
					mainController.setHomeScreen();
				} catch (IncorrectPasswordException e) {
					Alert alert = AlertCreator.createError("L'ancien mot de passe est incorrect.");
					alert.showAndWait();
				}
			}
		}
	}

}
