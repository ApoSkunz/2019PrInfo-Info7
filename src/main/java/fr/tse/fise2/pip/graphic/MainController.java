package fr.tse.fise2.pip.graphic;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.transitions.hamburger.HamburgerSlideCloseTransition;
import fr.tse.fise2.pip.Exceptions.IncorrectSyntax;
import fr.tse.fise2.pip.Exceptions.UserNotExistException;
import fr.tse.fise2.pip.Utils.Hashtag;
import fr.tse.fise2.pip.Utils.SearchUtils;
import fr.tse.fise2.pip.Utils.Session;
import fr.tse.fise2.pip.Utils.User;
import fr.tse.fise2.pip.Utils.Utils;
import fr.tse.fise2.pip.logger.MainLogger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controlleur principal de l'application. Il appel les differents controlleur
 * quand l'utilisateur interragi avec l'application.
 * 
 * @author Saunier Malo, Queffelec Matthieu
 */
public class MainController implements Initializable {

	private Logger logger = LogManager.getLogger(MainLogger.class.getName());
	private Stage primaryStage;
	private Session session = new Session(this);
	@FXML
	private BorderPane mainPane;
	@FXML
	private JFXTextField searchBar;
	@FXML
	private JFXButton connectionButton;
	@FXML
	private JFXButton registrationButton;
	@FXML
	private JFXButton buttonSearch;
	@FXML
	private JFXHamburger hamburgerButton;
	@FXML
	private JFXDrawer drawer;
	@FXML
	private JFXButton addToFavourites;

	public MainController(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	@FXML
	void handleSearchButtonAction(ActionEvent event) throws IncorrectSyntax {
		logger.info("Clique sur le bouton de recherche");
		String resultConcatPregMatch = Utils.resultConcatPregMatch(searchBar.getText(),
				"([#@]{1}[@# ]{1,})|([#@]{1}$)|(^[ ]{1,}$)");
		if (searchBar.getText().equals("")) {
			Alert alert = AlertCreator.createError("Vous devez entrer quelque chose dans la barre de recherche !");
			alert.showAndWait();
		} else if (!resultConcatPregMatch.equals("")) {
			Alert alert = AlertCreator.createError("Vous devez respecter les conditions d'utilisation !");
			alert.showAndWait();
			logger.warn("Rien n'est rentre dans la barre de recherche");
		} else {
			searchSceneBuilder();
		}
	}

	@FXML
	void keyPressed(KeyEvent event) throws IncorrectSyntax {
		if (event.getCode().toString().equals("ENTER")) {
			String resultConcatPregMatch = Utils.resultConcatPregMatch(searchBar.getText(),
					"([#@]{1}[@# ]{1,})|([#@]{1}$)|(^[ ]{1,}$)");
			logger.info("L'utilisateur a appuyer sur ENTREE");

			if (searchBar.getText().equals("")) {
				Alert alert = AlertCreator.createError("Vous devez entrer quelque chose dans la barre de recherche !");
				alert.showAndWait();
			} else if (!resultConcatPregMatch.equals("")) {
				Alert alert = AlertCreator.createError("Vous devez respecter les conditions d'utilisation !");
				alert.showAndWait();
			} else {
				searchSceneBuilder();
			}
		} else {
			notFavourited();
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initDrawer();
	}

	/**
	 * Methode qui initialise la toolbar.
	 */
	public void initDrawer() {
		logger.info("Initialisation du Drawer");
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainController.class.getResource("/fxml/toolbar.fxml"));
			ToolbarController controller = new ToolbarController(this);
			loader.setController(controller);
			VBox box = loader.load();
			drawer.setSidePane(box);
		} catch (IOException e) {
			e.printStackTrace();
		}

		HamburgerSlideCloseTransition transition = new HamburgerSlideCloseTransition(hamburgerButton);
		transition.setRate(-1);
		hamburgerButton.addEventHandler(MouseEvent.MOUSE_CLICKED, (e -> {
			transition.setRate(transition.getRate() * -1);
			transition.play();

			if (drawer.isShown()) {
				drawer.close();
				;
			} else {
				drawer.open();
			}
		}));
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur le bouton de connection.
	 * 
	 * @param event
	 */
	@FXML
	void connectClick(ActionEvent event) {
		logger.info("Clique de l'utilisateur sur le bouton de connexion");
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainController.class.getResource("/fxml/Connection.fxml"));
			ConnectionController sceneController = new ConnectionController(this.session, this);
			loader.setController(sceneController);
			// AnchorPane sceneLayout = (AnchorPane) loader.load();
			mainPane.setCenter(loader.load());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur le bouton de creation de
	 * compte.
	 * 
	 * @param event
	 */
	@FXML
	void registrateClick(ActionEvent event) {
		logger.info("Clique sur le bouton de creation de compte-deconnexion");
		if (this.session.isConnected()) {
			this.session.setConnected(false);
			setHomeScreen();
			logger.info("Deconnexion reussie");
		} else {
			try {
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(MainController.class.getResource("/fxml/Inscription.fxml"));
				InscriptionController sceneController = new InscriptionController(this.session, this);
				loader.setController(sceneController);
				mainPane.setCenter(loader.load());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur le bouton d'ajout aux
	 * favoris.
	 * 
	 * @param event
	 */
	@FXML
	void addFavouriteClick(ActionEvent event) {
		logger.info("Clique sur le bouton des favoris");
		if (session.isConnected()) {
			String text = searchBar.getText();
			if (session.getDatabase().isFavouritedBy(text, session)) {
				session.getDatabase().removeFavourites(text, session);
				notFavourited();
				Alert alert = AlertCreator.createInfo(searchBar.getText() + " a bien ete supprime des favoris.");
				alert.showAndWait();
			} else {
				String resultConcatPregMatch = Utils.resultConcatPregMatch(text,
						"([#@]{1}[@# ]{1,})|([#@]{1}$)|(^[ ]{0,}$)");
				if (!resultConcatPregMatch.equals("")) {
					Alert alert = AlertCreator.createError(
							"Vous devez respecter les conditions d'utilisation !\nUne barre de recherche ne peut pas etre vide!");
					alert.showAndWait();
				} else {
					try {
						session.getDatabase().addFavourites(searchBar.getText(), session);
						alreadyFavourited();
						Alert alert = AlertCreator.createInfo(searchBar.getText() + " a bien ete ajoute aux favoris.");
						alert.showAndWait();
					} catch (IncorrectSyntax incorrectSyntax) {
						Alert alert = AlertCreator.createError("La syntaxe est incorrecte.");
						alert.showAndWait();
					}
				}
			}

		} else {
			Alert alert = AlertCreator.createError("Vous n'etes pas connecte.");
			alert.showAndWait();
			logger.warn("L'utilisateur n'est pas connecte");
		}
	}

	/**
	 * Methode qui appelle le bon controlleur en fonction de la recherche effectuee
	 * par l'utilisateur.
	 * 
	 * @throws IncorrectSyntax
	 */
	public void searchSceneBuilder() throws IncorrectSyntax {
		String searchString = searchBar.getText();
		String identifier = searchString.substring(0, 1);
		String searchWords = searchString.substring(1);
		switch (identifier) {
		case "@":
			logger.info("Recherche d'un utilisateur");
			userStatsSceneBuilder(searchWords);
			break;
		case "#":
			logger.info("Recherche d'un hashtag");
			hashtagStatsSceneBuilder(searchString);
			break;
		default:
			logger.info("Recherche d'un mot");
			keywordStatsSceneBuilder(searchString);
			break;
		}
		if (session.isConnected()) {
			if (session.getDatabase().isFavouritedBy(searchString, session)) {
				logger.info("Recherche deja en favoris");
				alreadyFavourited();
			} else {
				notFavourited();
			}
		}
	}

	/**
	 * Methode qui appelle le controlleur affichant les statistiques d'un compte.
	 * 
	 * @param userToDisplay l'utilisateur a afficher.
	 * @throws IncorrectSyntax
	 */
	public void userStatsSceneBuilder(String userToDisplay) throws IncorrectSyntax {
		logger.info("Construction de l'affichage de la recherche d'utilisateur");
		try {
			User user = SearchUtils.makeSearch(userToDisplay);
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainController.class.getResource("/fxml/UserStats.fxml"));
			UserStatsController sceneController = new UserStatsController(primaryStage, this);
			loader.setController(sceneController);
			AnchorPane sceneLayout = (AnchorPane) loader.load();
			sceneController.setPanel(user);
			mainPane.setCenter(sceneLayout);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (UserNotExistException e) {
			Alert alert = AlertCreator.createError("Le compte n'existe pas");
			alert.showAndWait();
			e.printStackTrace();
		}

	}

	/**
	 * Methode mettant en place l'affichage d'un hashtag.
	 * 
	 * @param hashtagToDisplay le hashtag a afficher.
	 */
	public void hashtagStatsSceneBuilder(String hashtagToDisplay) {
		logger.info("Construction de l'affichage de la recherche d'un hashtag");
		Hashtag hashtag = new Hashtag(hashtagToDisplay);
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainController.class.getResource("/fxml/HashtagStats.fxml"));
			AnchorPane sceneLayout = (AnchorPane) loader.load();
			HashtagStatsController sceneController = loader.getController();
			sceneController.setPanel(hashtag);
			mainPane.setCenter(sceneLayout);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Methode mettant en place l'affichage d'un mot.
	 * 
	 * @param hashtagToDisplay le mot a afficher.
	 */
	public void keywordStatsSceneBuilder(String hashtagToDisplay) {
		logger.info("Construction de l'affichage de la recherche d'un mot clef");
		Hashtag keyword = new Hashtag(hashtagToDisplay);
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainController.class.getResource("/fxml/KeywordStats.fxml"));
			AnchorPane sceneLayout = (AnchorPane) loader.load();
			KeywordStatsController sceneController = loader.getController();
			sceneController.setPanel(keyword);
			mainPane.setCenter(sceneLayout);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Methode qui met en place l'affichage de la comparaison entre deux comptes.
	 * 
	 * @param user1 le premier compte.
	 * @param user2 le deuxieme compte.
	 */
	public void compareAccountsSceneBuilder(User user1, User user2) {
		logger.info("Construction de l'affichage de la comparaison de comptes");
		try {
			ScrollPane scrollPane = new ScrollPane();
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainController.class.getResource("/fxml/CompareAccounts.fxml"));
			AnchorPane sceneLayout = (AnchorPane) loader.load();
			CompareAccountsController sceneController = loader.getController();
			sceneController.setScene(user1, user2);
			scrollPane.setContent(sceneLayout);
			mainPane.setCenter(scrollPane);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Methode qui gere les changements de l'affichage lors d'une deconnexion.
	 */
	public void handleDisconnect() {
		final String IDLE_BUTTON_STYLE = "-fx-background-color: transparent;";
		final String HOVERED_BUTTON_STYLE = "-fx-background-color:  rgb(85,172,238); -fx-text-fill : rgb(215,218,224);";
		connectionButton.setText("Connexion");
		connectionButton.setDisable(false);
		connectionButton.setStyle("-fx-border-color:transparent;");
		registrationButton.setText("Inscription");
		notFavourited();
		connectionButton.setStyle(IDLE_BUTTON_STYLE);
		connectionButton.setOnMouseEntered(e -> connectionButton.setStyle(HOVERED_BUTTON_STYLE));
		connectionButton.setOnMouseExited(e -> connectionButton.setStyle(IDLE_BUTTON_STYLE));
		registrationButton.setOnMouseEntered(e -> registrationButton.setStyle(HOVERED_BUTTON_STYLE));
		registrationButton.setOnMouseExited(e -> registrationButton.setStyle(IDLE_BUTTON_STYLE));
	}

	/**
	 * Methode qui gere les changements de l'affichage lors d'une connection.
	 */
	public void handleConnect() {
		final String IDLE_BUTTON_STYLE = "-fx-text-fill:rgb(240, 52, 52);-fx-border-color:rgb(240, 52, 52);-fx-border-radius:5px;";
		final String HOVERED_BUTTON_STYLE = "-fx-border-color:rgb(215,218,224);-fx-border-radius:3px;-fx-background-color:rgb(240, 52, 52); -fx-text-fill:rgb(215,218,224);";
		connectionButton.setText("Connecte");
		connectionButton.setDisable(true);
		connectionButton
				.setStyle("-fx-text-fill:rgb(0, 230, 64);-fx-border-color:rgb(0, 230, 64);-fx-border-radius:5px;");
		registrationButton.setText("Deconnexion");
		registrationButton.setStyle(IDLE_BUTTON_STYLE);
		registrationButton.setOnMouseEntered(e -> registrationButton.setStyle(HOVERED_BUTTON_STYLE));
		registrationButton.setOnMouseExited(e -> registrationButton.setStyle(IDLE_BUTTON_STYLE));
	}

	/**
	 * Methode qui gere l'affichage lorsque qu'une recherche est deja en favoris.
	 */
	public void alreadyFavourited() {
		addToFavourites.setText("Retirer des favoris");
	}

	/**
	 * Methode qui gere l'affichage lorsque qu'une recherche n'est pas deja en
	 * favoris.
	 */
	public void notFavourited() {
		addToFavourites.setText("Ajouter aux favoris");
	}

	public BorderPane getMainPane() {
		return mainPane;
	}

	public Session getSession() {
		return session;
	}

	public TextField getSearchBar() {
		return searchBar;
	}

	/**
	 * Methode qui met la page sur un fond unicolore pour fermer les pages des
	 * connexions
	 */
	public void setHomeScreen() {
		AnchorPane layout = new AnchorPane();
		layout.setStyle("-fx-background-color : rgb(33,37,43)");
		mainPane.setCenter(layout);
	}

	public Stage getStage() {
		return primaryStage;
	}
}
