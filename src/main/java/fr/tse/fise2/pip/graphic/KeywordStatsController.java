package fr.tse.fise2.pip.graphic;

import fr.tse.fise2.pip.Exceptions.IncorrectSyntax;
import fr.tse.fise2.pip.Utils.APIUtils;
import fr.tse.fise2.pip.Utils.Hashtag;
import fr.tse.fise2.pip.Utils.SearchUtils;
import fr.tse.fise2.pip.Utils.Utils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.io.IOException;

/**
 * Controlleur qui fait le lien avec l'affichage des statistique d'un mot.
 * 
 * @author Queffelec Matthieu
 */
public class KeywordStatsController {

	@FXML
	private Label hashtagName;

	@FXML
	private Label tweetNumber;

	@FXML
	private Label userNumber;

	@FXML
	private TableView<Hashtag> hashtagtab;

	@FXML
	private TableColumn<Hashtag, String> numberColumn;

	@FXML
	private TableColumn<Hashtag, String> hashtagColumn;

	@SuppressWarnings("unused")
	private ObservableList<Hashtag> hashtagList = FXCollections.observableArrayList();
	@SuppressWarnings("unused")
	private ObservableList<Hashtag> numberList = FXCollections.observableArrayList();

	/**
	 * Methode qui met en place les composants avec les information contenu dans le
	 * POJO en parametre.
	 * 
	 * @param hashtag le POJO du mot a afficher.
	 */
	public void setPanel(Hashtag hashtag) {
		hashtagName.setText(hashtag.getName());
		tweetNumber.setText(String.valueOf(hashtag.getCount()));
		try {
			tweetNumber.setText(Integer.toString(Utils.CountTweets(APIUtils.getLastTweetsFrom(hashtag.getName()))));
			hashtagtab.setItems(Utils.toObservableList(SearchUtils.getMainHashtagsFrom(hashtag.getName())));
			hashtagColumn.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
			numberColumn.setCellValueFactory(cellData -> cellData.getValue().getCountProperty());
			userNumber.setText(String.valueOf(SearchUtils.getUniqueUserForHashtag(hashtag.getName())));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IncorrectSyntax incorrectSyntax) {
			incorrectSyntax.printStackTrace();
		}
	}

}
