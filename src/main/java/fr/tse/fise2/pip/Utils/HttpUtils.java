package fr.tse.fise2.pip.Utils;

import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.tse.fise2.pip.logger.MainLogger;

/**
 * Classe permettant de gerer les requetes HTTP pour les differentes recherches
 * sur l'API
 * 
 * @author Yammine Eric et Solane Alexandre
 *
 */
public class HttpUtils {
	Logger logger = LogManager.getLogger(MainLogger.class.getName());
	private HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();

	/**
	 * Methode permettant d'effectuer une requete HTTP GET avec les authorisations
	 * necessaires pour Twitter
	 * 
	 * @param rawURL L'url sur laquelle on veut effectuer la requete, sous forme
	 *               brute
	 * @return Le contenu de la reponse HTTP qui est sous la forme d'un String
	 *         parsable en Json
	 * @throws IOException          exception
	 * @throws InterruptedException exception
	 */
	public String getTwitterRequest(String rawURL) throws IOException, InterruptedException {
		logger.info("Envoi d'une requete HTTP de type GET vers l'url : " + rawURL);
		HttpRequest request = HttpRequest.newBuilder().GET().uri(URI.create(rawURL))
				.header("Authorization", "BEARER " + APIUtils.getBearerToken()).build();

		HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		logger.info("Requete envoyee avec succes, et reponse recuperee");
		return response.body();

	}

	public String postTwitterRequest(String rawURL, Map<Object, Object> data) throws IOException, InterruptedException {

		HttpRequest request = HttpRequest.newBuilder().POST(buildFormDataFromMap(data)).uri(URI.create(rawURL))
				.setHeader("User-Agent", "Java 11 HttpClient Bot")
				.header("Authorization",
						"BEARER AAAAAAAAAAAAAAAAAAAAACdhAgEAAAAAfx5mR3hFWVUB8B7IYScK%2Fr76zBQ%3DjYAXuUtIdcPhARPp7auspajkWqZVqmSEUDDNQmXSfGDdw2oKuM")// add
																																					// request
																																					// header
				.header("Content-Type", "application/x-www-form-urlencoded").build();

		HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		return response.body();

	}

	/**
	 * Methode permettant de generer le BearerToken
	 * 
	 * @param basicString String encode a partir des differentes "keys" de la classe
	 *                    APIUtils
	 * @return Le contenu de la reponse HTTP qui est sous la forme d'un String
	 *         parsable en Json
	 * @throws IOException          exception
	 * @throws InterruptedException exception
	 */
	public String bearerRequest(String basicString) throws IOException, InterruptedException {
		logger.info("Generation du bearer token afin de pouvoir utiliser l'API");
		// form parameters
		Map<Object, Object> data = new HashMap<Object, Object>();
		data.put("grant_type", "client_credentials");
		HttpRequest request = HttpRequest.newBuilder().POST(buildFormDataFromMap(data))
				.uri(URI.create("https://api.twitter.com/oauth2/token"))
				.setHeader("User-Agent", "Java 11 HttpClient Bot").header("Authorization", "Basic " + basicString)// add
																													// request
																													// header
				.header("Content-Type", "application/x-www-form-urlencoded").build();

		HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

		// print status code

		return response.body();

	}

	public HttpRequest.BodyPublisher buildFormDataFromMap(Map<Object, Object> data) {
		StringBuilder builder = new StringBuilder();
		for (Map.Entry<Object, Object> entry : data.entrySet()) {
			if (builder.length() > 0) {
				builder.append("&");
			}
			builder.append(URLEncoder.encode(entry.getKey().toString(), StandardCharsets.UTF_8));
			builder.append("=");
			builder.append(URLEncoder.encode(entry.getValue().toString(), StandardCharsets.UTF_8));
		}
		System.out.println(builder.toString());
		return HttpRequest.BodyPublishers.ofString(builder.toString());
	}
}
