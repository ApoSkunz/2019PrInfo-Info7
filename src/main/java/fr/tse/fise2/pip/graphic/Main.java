package fr.tse.fise2.pip.graphic;

/**
 * Classe qui appelle MainApp pour lancer l'application
 */
public class Main {
	public static void main(String[] args) {
		MainApp.main(args);
	}
}
