package fr.tse.fise2.pip.graphic;

import fr.tse.fise2.pip.Utils.Hashtag;
import fr.tse.fise2.pip.Utils.SearchUtils;
import fr.tse.fise2.pip.Utils.Utils;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.io.IOException;

/**
 * Classe qui fait le lien avec l'affichage des hashtags connexes du point
 * d'interet
 */
public class DisplayHobbiesHashtagController {

	@FXML
	private TableView<Hashtag> tableView;

	@FXML
	private TableColumn<Hashtag, String> nbHashtagColumn;

	@FXML
	private TableColumn<Hashtag, String> hashtagColumn;

	/**
	 * Methode qui met a jour les composants de l'affichage pour afficher les
	 * hashtag les plus twitter avec le point d'interet
	 * 
	 * @param hobby les elements du point d'interet
	 */
	public void setScene(String hobby) {
		try {
			tableView.setItems(Utils.toObservableList(SearchUtils.getRecurrentHashtagsFrom(hobby)));
			hashtagColumn.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
			nbHashtagColumn.setCellValueFactory(cellData -> cellData.getValue().getCountProperty());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
