package fr.tse.fise2.pip.Utils;

import fr.tse.fise2.pip.Database.Database;
import fr.tse.fise2.pip.graphic.MainController;

/**
 * Classe permettant de gerer la session de l'utilisateur en cours ( gerer les
 * connexion / deconnexions ), et verifier si un utilisateur est connecte
 * 
 * @author Yammine Eric, Solane Alexandre
 *
 */
public class Session {

	boolean isConnected = false;
	String username;
	int userID;
	Database db = new Database();
	MainController mainController;

	/**
	 * Constructeur permettant de creer une nouvelle session, associee au controleur
	 * graphique principal
	 * 
	 * @param controller c'est le controleur graphique qui va permettre de gerer
	 *                   l'affichage.
	 */

	public Session(MainController controller) {
		this.mainController = controller;
	}

	/**
	 * Methode permettant de connecter l'utilisateur a la session en affectant les
	 * champs dedies
	 * 
	 * @param username Nom de compte de l'utilisateur
	 * @param userID   Identifiant unique de l'utilisateur
	 */

	public void start(String username, int userID) {
		this.username = username;
		this.userID = userID;
		setConnected(true);
	}

	/**
	 * Methode permettant de savoir si un utilisateur est connecte
	 * 
	 * @return Booleen qui vaut vrai ou faux selon si l'utilisateur est connecte ou
	 *         pas
	 */
	public boolean isConnected() {
		return isConnected;
	}

	/**
	 * Methode permettant de "notifier" le controlleur graphique si l'utilisateur
	 * est connecte ou pas, se fait au moment de la connexion
	 * 
	 * @param isConnected Booleen qui permet de savoir si l'utilisateur est connecte
	 *                    ou non
	 */
	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
		if (isConnected) {
			mainController.handleConnect();
		} else {
			mainController.handleDisconnect();
		}
	}

	public String getUsername() {
		return username;
	}

	public int getUserID() {
		return userID;
	}

	public Database getDatabase() {
		return this.db;
	}

}
