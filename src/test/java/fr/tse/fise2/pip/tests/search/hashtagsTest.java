package fr.tse.fise2.pip.tests.search;

import java.io.IOException;
import java.util.SortedSet;

import org.junit.Assert;
import org.junit.Test;

import fr.tse.fise2.pip.Exceptions.IncorrectSyntax;
import fr.tse.fise2.pip.Exceptions.PrivateUserException;
import fr.tse.fise2.pip.Utils.Hashtag;
import fr.tse.fise2.pip.Utils.SearchUtils;

public class hashtagsTest {

	@Test
	/**
	 * Test unitaire pour verifier si la recherche des hashtags les plus populaires
	 * associes a un mot cle ou un autre hashtag, est effective
	 */
	public void mainHashtagsFromWordOrHashtag_test()
			throws IOException, InterruptedException, PrivateUserException, IncorrectSyntax {
		SortedSet<Hashtag> mainHashtagsUsed = SearchUtils.getMainHashtagsFrom("twitter");
		SortedSet<Hashtag> mainHashtagsUsed2 = SearchUtils.getMainHashtagsFrom("#twitter");
		Assert.assertTrue(mainHashtagsUsed.size() > 0 && mainHashtagsUsed2.size() > 0);
	}
}
