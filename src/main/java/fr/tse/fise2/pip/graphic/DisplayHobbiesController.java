package fr.tse.fise2.pip.graphic;

import fr.tse.fise2.pip.Utils.SearchUtils;
import fr.tse.fise2.pip.Utils.Tweet;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Iterator;
import java.util.SortedSet;

/**
 * Classe qui fait le lien avec l'affichage des statistiques des points
 * d'interets
 */
public class DisplayHobbiesController {

	@SuppressWarnings("unused")
	private MainController mainController;
	private Stage secondStage;
	private Stage primaryStage;
	private String hobbyItems;
	@FXML
	private TableView<Tweet> tableView;
	@FXML
	private TableColumn<Tweet, String> statsColumn;
	@FXML
	private TableColumn<Tweet, String> tweetsColumn;
	@FXML
	private Label tweetCount;
	@FXML
	private Label titleLabel;

	public DisplayHobbiesController(MainController mainController) {
		this.mainController = mainController;
		this.primaryStage = mainController.getStage();
	}

	/**
	 * Methode qui affiche la fenetre d'affichage des hashtag connexes au point
	 * d'interet.
	 * 
	 * @param event
	 */
	@FXML
	void displayHashtagClick(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(UserStatsController.class.getResource("/fxml/DisplayHobbiesHashtag.fxml"));
			DisplayHobbiesHashtagController displayHobbiesHashtagController = new DisplayHobbiesHashtagController();
			loader.setController(displayHobbiesHashtagController);
			AnchorPane layout = loader.load();
			Scene secondScene = new Scene(layout);
			displayHobbiesHashtagController.setScene(hobbyItems);
			secondStage = new Stage();
			secondStage.setTitle("Hashtags recurrents du point d'interet");
			secondStage.setScene(secondScene);

			// Specifies the modality for new window.
			secondStage.initModality(Modality.WINDOW_MODAL);

			// Specifies the owner Window (parent) for new window
			secondStage.initOwner(primaryStage);

			// Set position of second window, related to primary window.
			secondStage.setX(primaryStage.getX() + 200);
			secondStage.setY(primaryStage.getY() + 100);

			secondStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Methode qui met a jour les composants de l'affichage pour afficher les
	 * statistiques des 10 tweets les plus populaires correspondant au point
	 * d'interet
	 * 
	 * @param hobby elements du POI
	 */
	public void setScene(String hobby) {
		try {
			this.hobbyItems = hobby;
			SortedSet<Tweet> tweets = SearchUtils.getMainTweetsFrom(hobby);
			tweetCount.setText(tweetCount.getText() + tweets.size());
			ObservableList<Tweet> tweetsArray = FXCollections.observableArrayList();
			Iterator<Tweet> it = tweets.iterator();
			int i = 0;
			while (it.hasNext() && i < 10) {
				tweetsArray.add(it.next());
				i++;
			}
			tableView.setItems(tweetsArray);
			statsColumn.setCellValueFactory(
					cellData -> new SimpleStringProperty("RT : " + ((Math.round(cellData.getValue().getRetweetCount()))
							+ "\nFav : " + Math.round(cellData.getValue().getFavoriteCount()))));
			tweetsColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTweetText()));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}