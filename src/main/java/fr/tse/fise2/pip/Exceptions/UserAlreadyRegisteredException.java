package fr.tse.fise2.pip.Exceptions;

import java.lang.Exception;

/**
 * Exception permettant de gerer le cas ou l'utilisateur est deja enregistre
 * dans la database (double inscription)
 * 
 * @author Yammine Eric
 *
 */

@SuppressWarnings("serial")
public class UserAlreadyRegisteredException extends Exception {
	public UserAlreadyRegisteredException() {
		super();
	}
}