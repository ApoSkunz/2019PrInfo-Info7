package fr.tse.fise2.pip.Utils;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * POJO Hashtag permettant de creer et "stocker" les donnees d'un Hashtag
 * Twitter (le nom du hashtag et le nombre d'occurences)
 *
 * @author Yammine Eric et Solane Alexandre
 */
public class Hashtag implements Comparable<Hashtag> {
	private String name;
	private Double count = (double) 0;

	public Hashtag(String name, Double count) {
		this.name = name;
		this.count = count;
	}

	public Hashtag(String name) {
		this.name = name;
	}

	public Hashtag() {
	}

	public void incrementCount(int n) {
		this.count += n;
	}

	public String getName() {
		return name;
	}

	public String getHashtagName() {
		return "#" + name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getCount() {
		return count;
	}

	public void setCount(Double count) {
		this.count = count;
	}

	public StringProperty getNameProperty() {
		return new SimpleStringProperty("#" + name);
	}

	public StringProperty getCountProperty() {
		return new SimpleStringProperty(Integer.toString((int) Math.round(count)));
	}

	@Override
	public String toString() {
		return "Hashtag [name=" + name + ", count=" + count + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Hashtag other = (Hashtag) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int compareTo(Hashtag h) {
		int countComp = -Double.compare(this.count, h.getCount());
		if (countComp != 0)
			return countComp;
		else
			return this.name.compareTo(h.getName());
	}

}
