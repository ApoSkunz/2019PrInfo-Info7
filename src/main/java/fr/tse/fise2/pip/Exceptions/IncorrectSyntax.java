package fr.tse.fise2.pip.Exceptions;

/**
 * Exception permettant de gerer le cas ou l'utilisateur saisi un favoris ou
 * hobby avec des ensembles @ ou # vides
 * 
 * @author Yammine Eric
 *
 */
@SuppressWarnings("serial")
public class IncorrectSyntax extends Exception {
	public IncorrectSyntax() {
		super();
	}
}
