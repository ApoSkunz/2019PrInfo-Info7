package fr.tse.fise2.pip.Utils;

import com.google.gson.JsonArray;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe permettant d'offrir des outils utiles aux dveloppeurs, afin de
 * manipuler les diffrents objets avec plus de facilit
 *
 * @author Yammine Eric, Solane Alexandre
 */

public class Utils {
	/**
	 * Mthode permettant de limiter la liste de Hashtag les plus utiliss aux "n"
	 * premiers, en ajoutant le Hashtag "Other" qui va prendre en compte ceux qu'on
	 * a "coups"
	 *
	 * @param list La liste de Hashtag que l'on veut limiter
	 * @param n    le nombre de hashtags que l'on veut garder dans la liste
	 * @return Liste de hashtag limite aux n premiers (les tops hashtags) , avec le
	 *         hashtag "Other" qui regroupe ceux que l'on a laisss de ct
	 */
	public static Set<Hashtag> setLimit(Set<Hashtag> list, int n) {
		Set<Hashtag> returnList = new TreeSet<Hashtag>();
		if (list.size() < n) {
			returnList = list;
		} else {
			Iterator<Hashtag> it = list.iterator();
			int i = 0;
			while (i < n - 1) {
				returnList.add(it.next());
				i++;
			}
			Double count = (double) 0;
			while (it.hasNext()) {
				count += it.next().getCount();
			}
			Hashtag other = new Hashtag("Other", count);
			returnList.add(other);
		}
		return returnList;
	}

	/**
	 * Methode qui ajoute a chaque Hashtag dans le set en parametre le pourcentage
	 * correspondant a la presence du hashtag dans le set
	 * 
	 * @param list Set avec les hashtag a traiter
	 * @return set avec les hashtag et leur pourcentage
	 */
	public static Set<Hashtag> setPercentage(Set<Hashtag> list) {
		int somme = 0;
		Iterator<Hashtag> it = list.iterator();
		while (it.hasNext()) {
			Hashtag hashtag = it.next();
			somme += hashtag.getCount();
		}
		it = list.iterator();
		while (it.hasNext()) {
			Hashtag hashtag = it.next();
			hashtag.setCount(hashtag.getCount() / somme * 100);
		}
		return list;
	}

	/**
	 * Mthode permettant de hasher un mot de passe de l'utilisateur afin de pouvoir
	 * le stocker dans la Database
	 *
	 * @param passwordToHash Le mot de passe hasher
	 * @param salt           String sel ajouter permettant de scuriser le hash
	 * @return hashedPassword String envoie le mot de passe hash
	 */
	public static String hashPassword(String passwordToHash, String salt) {
		String hashedPassword = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.update(salt.getBytes(StandardCharsets.UTF_8));
			byte[] bytes = md.digest(passwordToHash.getBytes(StandardCharsets.UTF_8));
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}

			hashedPassword = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hashedPassword;
	}

	/**
	 * Mthode qui va renvoyer le nombre de tweets compris dans une tweetList
	 *
	 * @param tweetList L'array comportant les tweets que l'on veut compter
	 * @return Le nombre de tweets prsents dans cet array
	 */
	public static int CountTweets(JsonArray tweetList) {

		return tweetList.size();

	}

	/**
	 * Methode qui renvoie le set passe en parametre sous la forme d'une
	 * observableList pour l'afficher graphiquement.
	 * 
	 * @param set Set contenant les hashtags a afficher
	 * @return la ObservableList contenant les hashtag a afficher
	 */
	public static ObservableList<Hashtag> toObservableList(Set<Hashtag> set) {
		set = Utils.setLimit(set, 11);
		Iterator<Hashtag> it = set.iterator();
		Hashtag other = new Hashtag();
		while (it.hasNext()) {
			Hashtag hashtag = it.next();
			if (hashtag.getName().equals("Other")) {
				other = hashtag;
			}
		}
		set.remove(other);
		ObservableList<Hashtag> observableList = FXCollections.observableArrayList();
		it = set.iterator();
		while (it.hasNext()) {
			observableList.add(it.next());
		}
		return observableList;
	}

	/**
	 * Methode permettant d'avoir le resultat concatener d'un pregmatch sur une
	 * string
	 * 
	 * @param search recherche sur laquelle on souhaite faire un pregmatch
	 * @param regex  expression reguliere
	 * @return le resultat de la concatenation d'un pregmatch sur une string
	 */
	public static String resultConcatPregMatch(String search, String regex) {
		Pattern patternFavourites = Pattern.compile(regex);
		Matcher matcherFavourites = patternFavourites.matcher(search);
		String resultConcatPregMatch = new String();
		long pipeCount = regex.chars().filter(ch -> ch == '|').count();
		while (matcherFavourites.find()) {
			for (int groupNumber = 0; groupNumber < pipeCount + 1; groupNumber++) {
				resultConcatPregMatch += matcherFavourites.group(groupNumber + 1);
			}
		}
		return resultConcatPregMatch;
	}
}
