package fr.tse.fise2.pip.graphic;

import fr.tse.fise2.pip.Exceptions.PrivateUserException;
import fr.tse.fise2.pip.Utils.SearchUtils;
import fr.tse.fise2.pip.Utils.User;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

import static fr.tse.fise2.pip.graphic.UserStatsController.setPieChart;

/**
 * Controlleur qui fait le lien avec l'interface graphique qui affiche la
 * comparaison entre deux comptes.
 *
 * @author Queffelec Matthieu
 */
public class CompareAccountsController {

	@FXML
	private AnchorPane navigationPane;

	@FXML
	private ImageView bannerView;

	@FXML
	private ImageView profilePictureView;

	@FXML
	private Label userNameLabel;

	@FXML
	private Label screenNameLabel;

	@FXML
	private PieChart pieChart;

	@FXML
	private Label tweetsLabel;

	@FXML
	private Label followersLabel;

	@FXML
	private Label followedLabel;

	@FXML
	private ImageView bannerView2;

	@FXML
	private ImageView profilePictureView2;

	@FXML
	private Label userNameLabel2;

	@FXML
	private Label screenNameLabel2;

	@FXML
	private Label tweetsLabel2;

	@FXML
	private Label followersLabel2;

	@FXML
	private Label followedLabel2;

	@FXML
	private PieChart pieChart1;

	@FXML
	private Label tweetText;

	@FXML
	private Label tweetDiff;

	@FXML
	private Label followerText;

	@FXML
	private Label followerDiff;

	@FXML
	private Label friendsText;

	@FXML
	private Label friendsDiff;

	/**
	 * Methode qui met en place l'affichage en allant chercher les information dans
	 * les POJO.
	 *
	 * @param user1 le premier compte a afficher.
	 * @param user2 le deuxieme compte a afficher.
	 */
	public void setScene(User user1, User user2) {
		String follower, tweets, followed;
		String user1name = "@" + user1.getScreen_name();
		String user2name = "@" + user2.getScreen_name();
		int diffFollowers = user1.getFollowers_count() - user2.getFollowers_count();
		int diffTweets = user1.getStatuses_count() - user2.getStatuses_count();
		int diffFriends = user1.getFriends_count() - user2.getFriends_count();
		if (diffFollowers > 0) {
			follower = "plus";
			followerDiff.setStyle("-fx-text-fill:rgb(90, 161, 57);");
			followerDiff.setText("+" + diffFollowers);
		} else if (diffFollowers < 0) {
			follower = "moins";
			followerDiff.setStyle("-fx-text-fill:rgb(240, 52, 52);");
			followerDiff.setText(String.valueOf(diffFollowers));
		} else {
			follower = "autant";
			followerDiff.setText(String.valueOf(diffFollowers));
		}
		if (diffTweets > 0) {
			tweets = "plus";
			tweetDiff.setStyle("-fx-text-fill:rgb(90, 161, 57);");
			tweetDiff.setText("+" + diffTweets);
		} else if (diffTweets < 0) {
			tweets = "moins";
			tweetDiff.setStyle("-fx-text-fill:rgb(240, 52, 52);");
			tweetDiff.setText(String.valueOf(diffTweets));
		} else {
			tweets = "autant";
			tweetDiff.setText(String.valueOf(diffTweets));
		}
		if (diffFriends > 0) {
			followed = "plus";
			friendsDiff.setStyle("-fx-text-fill:rgb(90, 161, 57);");
			friendsDiff.setText("+" + diffFriends);
		} else if (diffFriends < 0) {
			followed = "moins";
			friendsDiff.setStyle("-fx-text-fill:rgb(240, 52, 52);");
			friendsDiff.setText(String.valueOf(diffFriends));
		} else {
			followed = "autant";
			friendsDiff.setText(String.valueOf(diffFriends));
		}
		followerText.setText(user1name + " a " + follower + " de followers que  " + user2name + ".");
		tweetText.setText(user1name + " a ecris " + tweets + " de tweets que " + user2name + ".");
		friendsText.setText(user1name + " suit " + followed + " de personnes que " + user2name + ".");

		Image banner = new Image(user1.getProfile_banner_url());
		Image profilePicture = new Image(user1.getProfile_image_url());
		bannerView.setImage(banner);
		profilePictureView.setImage(profilePicture);
		userNameLabel.setText(user1.getName());
		screenNameLabel.setText("@" + user1.getScreen_name());
		followersLabel.setText(user1.getFollowers_count().toString());
		followedLabel.setText(user1.getFriends_count().toString());
		tweetsLabel.setText(user1.getStatuses_count().toString());

		Image banner2 = new Image(user2.getProfile_banner_url());
		Image profilePicture2 = new Image(user2.getProfile_image_url());
		bannerView2.setImage(banner2);
		profilePictureView2.setImage(profilePicture2);
		userNameLabel2.setText(user2.getName());
		screenNameLabel2.setText("@" + user2.getScreen_name());
		followersLabel2.setText(user2.getFollowers_count().toString());
		followedLabel2.setText(user2.getFriends_count().toString());
		tweetsLabel2.setText(user2.getStatuses_count().toString());
		try {
			pieChart = setPieChart(SearchUtils.getMainHashtagsFrom(user1), pieChart);
			pieChart1 = setPieChart(SearchUtils.getMainHashtagsFrom(user2), pieChart1);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (PrivateUserException e) {
			e.printStackTrace();
			Alert alert = AlertCreator.createError("L'utiliateur est prive, les hashtags ne peuvent pas etre affiche");
			alert.showAndWait();
		}

	}

}