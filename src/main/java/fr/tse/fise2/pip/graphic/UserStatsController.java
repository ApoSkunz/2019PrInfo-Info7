package fr.tse.fise2.pip.graphic;

import fr.tse.fise2.pip.Exceptions.PrivateUserException;
import fr.tse.fise2.pip.Utils.Hashtag;
import fr.tse.fise2.pip.Utils.SearchUtils;
import fr.tse.fise2.pip.Utils.User;
import fr.tse.fise2.pip.Utils.Utils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

/**
 * Controlleur qui fait le lien avec l'affichage des statistiques d'un
 * utilisateur
 * 
 * @author Queffelec Matthieu
 */
public class UserStatsController {

	private MainController mainController;

	private Stage primaryStage;

	private Stage secondStage;

	private User user1 = null;

	@FXML
	private AnchorPane navigationPane;

	@FXML
	private ImageView bannerView;

	@FXML
	private ImageView profilePictureView;

	@FXML
	private Label userNameLabel;

	@FXML
	private Label screenNameLabel;

	@FXML
	private Label tweetsLabel;

	@FXML
	private Label followersLabel;

	@FXML
	private Label followedLabel;

	@FXML
	private PieChart pieChart;

	@FXML
	private Button comparebtn;

	public UserStatsController(Stage primaryStage, MainController mainController) {
		this.primaryStage = primaryStage;
		this.mainController = mainController;
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur le bouton de comparaison de
	 * deux compte. Ouvre le popup pour permettre a l'utilisateur de rentrer le
	 * deuxieme compte.
	 * 
	 * @param event
	 */
	@FXML
	void compareClick(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(UserStatsController.class.getResource("/fxml/ComparePopup.fxml"));
			PopupController popupController = new PopupController(this);
			loader.setController(popupController);
			AnchorPane layout = loader.load();
			Scene secondScene = new Scene(layout);
			secondStage = new Stage();
			secondStage.setTitle("Choose Second Account");
			secondStage.setScene(secondScene);

			// Specifies the modality for new window.
			secondStage.initModality(Modality.WINDOW_MODAL);

			// Specifies the owner Window (parent) for new window
			secondStage.initOwner(primaryStage);

			// Set position of second window, related to primary window.
			secondStage.setX(primaryStage.getX() + 200);
			secondStage.setY(primaryStage.getY() + 100);

			secondStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Methode qui met en place les composants pour afficher les informations de
	 * l'utiisateur a afficher
	 * 
	 * @param userToDisplay POJO contenant les information de l'utilisateur a
	 *                      afficher.
	 * @throws IOException          exception
	 * @throws InterruptedException exception
	 */
	public void setPanel(User userToDisplay) throws IOException, InterruptedException {
		user1 = userToDisplay;
		Image banner = new Image(userToDisplay.getProfile_banner_url());
		Image profilePicture = new Image(userToDisplay.getProfile_image_url());
		bannerView.setImage(banner);
		profilePictureView.setImage(profilePicture);
		userNameLabel.setText(userToDisplay.getName());
		screenNameLabel.setText("@" + userToDisplay.getScreen_name());
		followersLabel.setText(userToDisplay.getFollowers_count().toString());
		followedLabel.setText(userToDisplay.getFriends_count().toString());
		tweetsLabel.setText(userToDisplay.getStatuses_count().toString());
		try {
			pieChart = setPieChart(SearchUtils.getMainHashtagsFrom(userToDisplay), pieChart);
		} catch (PrivateUserException e) {
			e.printStackTrace();
			Alert alert = AlertCreator.createError("L'utiliateur est prive, les hashtags ne peuvent pas etre affiches");
			alert.showAndWait();
		}
	}

	/**
	 * Methode permettant de mettre en place le graphique en fonction de la liste de
	 * hashtag fourni.
	 * 
	 * @param list     la liste des hashtag a afficher
	 * @param pieChart le graphique a mettre a jour.
	 * @return graphique camembert
	 */
	public static PieChart setPieChart(Set<Hashtag> list, PieChart pieChart) {
		list = Utils.setLimit(list, 10);
		list = Utils.setPercentage(list);
		Iterator<Hashtag> it = list.iterator();
		while (it.hasNext()) {
			Hashtag hashtag = it.next();
			PieChart.Data slice = new PieChart.Data(hashtag.getName(), hashtag.getCount());
			pieChart.getData().add(slice);
		}
		pieChart.getData().forEach(data -> {
			@SuppressWarnings("static-access")
			String percentage = new String().format("%.2f%%", data.getPieValue());
			Tooltip toolTip = new Tooltip(percentage);
			Tooltip.install(data.getNode(), toolTip);
		});
		return pieChart;
	}

	/**
	 * Methode qui appelle la methode pour afficher la comparaison des deux comptes
	 * 
	 * @param user2 deuxieme utilisateur
	 */
	public void compareAccounts(User user2) {
		secondStage.close();
		mainController.compareAccountsSceneBuilder(user1, user2);
	}
}
