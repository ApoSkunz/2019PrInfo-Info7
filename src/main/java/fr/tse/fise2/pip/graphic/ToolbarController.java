package fr.tse.fise2.pip.graphic;

import com.jfoenix.controls.JFXButton;
import fr.tse.fise2.pip.Utils.Session;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

/**
 * Controlleur faisant le lien entre l'interface graphique de la toolbar et les
 * differents menus disponibles.
 * 
 * @author Saunier Malo, Queffelec Matthieu
 */
public class ToolbarController {

	private MainController mainController;
	@FXML
	private JFXButton myAccountButton;
	@FXML
	private JFXButton favouritesButton;
	@FXML
	private JFXButton POIbutton;

	public ToolbarController(MainController mainController) {
		this.mainController = mainController;
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur le bouton d'affichage du
	 * compte
	 * 
	 * @param event
	 */
	@FXML
	void accountClick(ActionEvent event) {
		Session session = mainController.getSession();
		if (session.isConnected()) {
			try {
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(ToolbarController.class.getResource("/fxml/Account.fxml"));
				AccountController sceneController = new AccountController(mainController);
				loader.setController(sceneController);
				AnchorPane sceneLayout = (AnchorPane) loader.load();
				sceneController.setPanel();
				mainController.getMainPane().setCenter(sceneLayout);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			Alert alert = AlertCreator.createError("Vous devez etre connecte.");
			alert.showAndWait();
		}
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur le bouton des favoris
	 * 
	 * @param event
	 */
	@FXML
	void favouritesClick(ActionEvent event) {
		Session session = mainController.getSession();
		if (session.isConnected()) {
			try {
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(ToolbarController.class.getResource("/fxml/Favourites.fxml"));
				FavouritesController sceneController = new FavouritesController(mainController);
				loader.setController(sceneController);
				AnchorPane sceneLayout = (AnchorPane) loader.load();
				sceneController.setScene(session.getDatabase().displayFavourites(session));
				mainController.getMainPane().setCenter(sceneLayout);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			Alert alert = AlertCreator.createError("Vous devez etre connecte.");
			alert.showAndWait();
		}
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur le bouton des Points
	 * d'interets
	 * 
	 * @param event
	 */
	@FXML
	void POIClick(ActionEvent event) {
		Session session = mainController.getSession();
		if (session.isConnected()) {
			try {
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(ToolbarController.class.getResource("/fxml/Hobbies.fxml"));
				HobbiesController sceneController = new HobbiesController(mainController);
				loader.setController(sceneController);
				AnchorPane sceneLayout = (AnchorPane) loader.load();
				sceneController.setScene(session.getDatabase().displayHobbies(session));
				mainController.getMainPane().setCenter(sceneLayout);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			Alert alert = AlertCreator.createError("Vous devez etre connecte.");
			alert.showAndWait();
		}
	}

}
