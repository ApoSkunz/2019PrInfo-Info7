package fr.tse.fise2.pip.Utils;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Base64;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Classe permettant de gerer les requetes liees a l'API
 * 
 * @author Yammine Eric et Solane Alexandre
 *
 */

public class APIUtils {
	private static String apikey = "r8g4m78baH95mQI9UhBex8zm9";
	private static String apisecretkey = "NxrnHlJJhP1LfZpyhgM5CbId6qnJscUfJt8ALLrZ0q6RxHrvYa";
	private static String accesstoken = "1188855589205618688-g6w4OrkdxTY8WvDOyTLgEazvPPTpEx";
	private static String accesstokensecret = "e5WnngzdLpTK6zyT3DUsSFinVM2i8SuFGjfO68Zt4BzsR";
	private static String bearerToken = "AAAAAAAAAAAAAAAAAAAAACdhAgEAAAAAfx5mR3hFWVUB8B7IYScK%2Fr76zBQ%3DjYAXuUtIdcPhARPp7auspajkWqZVqmSEUDDNQmXSfGDdw2oKuM";
	private static HttpUtils httpUtils = new HttpUtils();

	public static JsonObject search(String toBeFound) throws IOException, InterruptedException {
		String urlToSearch = "https://api.twitter.com/1.1/users/show.json?screen_name=" + toBeFound;
		String response = httpUtils.getTwitterRequest(urlToSearch);
		JsonObject jsonObject = JsonParser.parseString(response).getAsJsonObject();
		return jsonObject;
	}

	// Fonction qui renvoie le BearerToken avec les keys actuelles
	/**
	 * Methode permettant generer le BearerToken selon les champs entrees dans la
	 * classe
	 * 
	 * @return BearerToken a utiliser pour effectuer les differentes requetes sans
	 *         authentification utilisateur
	 * @throws IOException          exception
	 * @throws InterruptedException exception
	 */
	public static String makeBearerToken() throws IOException, InterruptedException {
		StringBuilder urltoencode = new StringBuilder();
		urltoencode.append(apikey);
		urltoencode.append(":");
		urltoencode.append(apisecretkey);
		String basic = Base64.getEncoder().encodeToString(urltoencode.toString().getBytes());
		String bToken = httpUtils.bearerRequest(basic);
		JsonObject bearerTokenJson = JsonParser.parseString(bToken).getAsJsonObject();
		bToken = bearerTokenJson.get("access_token").toString();
		return bToken;

	}

	/**
	 * Methode permettant de recuperer une "liste" des derniers tweets effectues par
	 * un utilisateur
	 * 
	 * @param user Utilisateur sur lequel on veut effectuer la recherche
	 * @return JsonArray contenant les differents tweets de l'utilisateur sous forme
	 *         de JsonObject
	 * @throws IOException          exception
	 * @throws InterruptedException exception
	 */
	public static JsonArray getLastTweetsFrom(User user) throws IOException, InterruptedException {
		String screenName = user.getScreen_name();
		String urlToSearch = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=" + screenName
				+ "&count=200&include_replies=true&include_rts=true&trim_user=true&tweet_mode=extended";
		String response = httpUtils.getTwitterRequest(urlToSearch);
		JsonArray jsonArray = JsonParser.parseString(response).getAsJsonArray();
		return jsonArray;

	}

	/**
	 * Methode permettant de recuperer une "liste" des derniers tweets lies a un
	 * hashtag ou un centre d'interet
	 * 
	 * @param wordOrHashtag Le centre d'interet ou le Hashtag a rechercher sous
	 *                      forme d'un String
	 * @return JsonArray contenant les differents tweets lies a la recherche sous
	 *         forme de JsonObject
	 * @throws IOException          exception
	 * @throws InterruptedException exception
	 */
	public static JsonArray getLastTweetsFrom(String wordOrHashtag) throws IOException, InterruptedException {
		String encodedSearch = URLEncoder.encode(wordOrHashtag, "UTF-8");
		String urlToSearch = "https://api.twitter.com/1.1/search/tweets.json?q=" + encodedSearch
				+ "&count=100&tweet_mode=extended";
		String response = httpUtils.getTwitterRequest(urlToSearch);
		JsonArray jsonArray = JsonParser.parseString(response).getAsJsonObject().get("statuses").getAsJsonArray();
		return jsonArray;

	}

	public String getApikey() {
		return apikey;
	}

	public String getApisecretkey() {
		return apisecretkey;
	}

	public String getAccesstoken() {
		return accesstoken;
	}

	public String getAccesstokensecret() {
		return accesstokensecret;
	}

	public static String getBearerToken() {
		return bearerToken;
	}
}
