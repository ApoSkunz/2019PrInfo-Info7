package fr.tse.fise2.pip.Utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Classe permettant de gerer et recuperer des donnees des objets Json
 * 
 * @author Yammine Eric et Solane Alexandre
 *
 */
public class JsonUtils {

	/**
	 * Methode permettant de recuperer les Hashtags utilises dans un tweet
	 * 
	 * @param tweetObject Objet Json contenant les informations d'un tweet
	 * @return Liste contenant les hashtags utilises dans le tweet(Json) passe en
	 *         parametre
	 */
	public static ArrayList<Hashtag> getHashtagsFromTweet(JsonObject tweetObject) {
		ArrayList<Hashtag> hashtagList = new ArrayList<Hashtag>();
		JsonArray hashtags = tweetObject.get("entities").getAsJsonObject().get("hashtags").getAsJsonArray();

		if (tweetObject.has("retweeted_status")) {
			hashtags = tweetObject.get("retweeted_status").getAsJsonObject().get("entities").getAsJsonObject()
					.get("hashtags").getAsJsonArray();
		}

		for (int i = 0; i < hashtags.size(); i++) {
			Hashtag hashtag = new Hashtag(hashtags.get(i).getAsJsonObject().get("text").getAsString());
			hashtagList.add(hashtag);
		}
		return hashtagList;
	}

	/**
	 * Methode permettant d'obtenir les informations sur le tweet
	 * 
	 * @param tweetObject tweet sous forme Json
	 * @return Tweet correspondant au POJO
	 */
	public static Tweet getTweetFromTweet(JsonObject tweetObject) {
		String screen_name = tweetObject.get("user").getAsJsonObject().get("screen_name").getAsString();
		String tweetText = tweetObject.get("full_text").getAsString();
		Double favoriteCount = tweetObject.get("favorite_count").getAsDouble();
		Double retweetCount = tweetObject.get("retweet_count").getAsDouble();

		Tweet tweet = new Tweet(screen_name, tweetText, favoriteCount, retweetCount);

		return tweet;
	}

	/**
	 * Methode permettant de recuperer les Hashtags utilises dans une liste de
	 * Tweets
	 * 
	 * @param tweetList     JsonArray contenant une liste de tweets sous forme de
	 *                      Json
	 * @param wordOrHashtag recherche du favoris, du POI ou de la recherche sur
	 *                      l'app
	 * @return Liste contenant les Hashtags utilises dans tous les tweets du
	 *         JsonArray passe en parametres
	 */
	public static ArrayList<Hashtag> getHashtagsFromTweetList(JsonArray tweetList, String wordOrHashtag) {
		ArrayList<Hashtag> hashtags = new ArrayList<Hashtag>();
		for (int i = 0; i < tweetList.size(); i++) {
			ArrayList<Hashtag> hashtagList = getHashtagsFromTweet(tweetList.get(i).getAsJsonObject());
			for (int index = 0; index < hashtagList.size(); index++) {
				if (hashtagList.get(index).getName() != "") {
					String nameHashtag = hashtagList.get(index).getName();
					if (wordOrHashtag.indexOf(nameHashtag) == -1) {
						hashtags.add(hashtagList.get(index));
					}
				}

			}
		}
		return hashtags;
	}

	/**
	 * Methode permettant de recuperer les Tweets sous forme de POJO utilises dans
	 * une liste de Tweets sous forme de JsonArray
	 * 
	 * @param tweetList liste de tweet au format JsonArray
	 * @return liste de Tweet au format POJO
	 */
	public static ArrayList<Tweet> getTweetsFromTweetList(JsonArray tweetList) {
		ArrayList<Tweet> tweets = new ArrayList<Tweet>();
		for (int i = 0; i < tweetList.size(); i++) {
			Tweet tweetListOnSearch = getTweetFromTweet(tweetList.get(i).getAsJsonObject());
			tweets.add(tweetListOnSearch);
		}
		return tweets;
	}

	/**
	 * Methode permettant d'avoir une liste d'utilisateurs uniques parmis une liste
	 * de Tweets
	 * 
	 * @param tweetList JsonArray contenant une liste de tweets sous forme de Json
	 * @return Set contenant les nom d'utilisateurs presents dans la liste de
	 *         Tweets, sans duplicats
	 */
	public static Set<String> getUniqueUserFromTweetList(JsonArray tweetList) {
		Set<String> uniqueUserId = new HashSet<String>();
		for (int i = 0; i < tweetList.size(); i++) {
			uniqueUserId.add(
					tweetList.get(i).getAsJsonObject().get("user").getAsJsonObject().get("screen_name").getAsString());
		}
		return uniqueUserId;
	}

}
