package fr.tse.fise2.pip.graphic;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import fr.tse.fise2.pip.Exceptions.IncorrectPasswordException;
import fr.tse.fise2.pip.Exceptions.UserNotExistException;
import fr.tse.fise2.pip.Utils.Session;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;

/**
 * Controlleur qui fait le lien avec l'affichage de la page de connexion.
 * 
 * @author Queffelec Matthieu
 */
public class ConnectionController {

	private Session session;
	private MainController mainController;

	@FXML
	private JFXTextField loginField;

	@FXML
	private JFXPasswordField passwordField;

	@FXML
	private JFXButton validateConnectionButton;

	public ConnectionController(Session session, MainController mainController) {
		this.session = session;
		this.mainController = mainController;
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur le bouton connexion. Elle
	 * verifie que l'utilisateur existe et le connecte si le mode de passe est le
	 * bon.
	 * 
	 * @param event
	 */
	@FXML
	void connectionclick(ActionEvent event) {
		try {
			this.session.getDatabase().loginAccount(loginField.getText(), passwordField.getText(), this.session);
			mainController.setHomeScreen();
		} catch (UserNotExistException e) {
			Alert alert = AlertCreator.createError("L'utilisateur n'existe pas !");
			alert.showAndWait();
		} catch (IncorrectPasswordException e) {
			Alert alert = AlertCreator.createError("Le mot de passe est incorrect.");
			alert.showAndWait();
		}
	}

}