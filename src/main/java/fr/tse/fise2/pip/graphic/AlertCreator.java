package fr.tse.fise2.pip.graphic;

import javafx.scene.control.Alert;

/**
 * Classe qui genere des fenetre d'erreur ou d'information.
 * 
 * @author Queffelec Matthieu
 */
public class AlertCreator {
	/**
	 * Methode statique qui genere une Alert de type ERROR afin d'afficher l'erreur
	 * a l'utilisateur.
	 * 
	 * @param error String contenant le message a afficher a l'utilisateur.
	 * @return Une alerte avec le message a afficher.
	 */
	public static Alert createError(String error) {
		Alert alert = new Alert(Alert.AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText(null);
		alert.setContentText(error);
		return alert;
	}

	/**
	 * Methode statique qui genere une Alert de type INFORMATION afin d'informaer
	 * l'utilisateur.
	 * 
	 * @param info String contenant l'information.
	 * @return Alert contenant le message a afficher.
	 */
	public static Alert createInfo(String info) {
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("Info");
		alert.setHeaderText(null);
		alert.setContentText(info);
		return alert;
	}

}
