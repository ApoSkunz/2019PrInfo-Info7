package fr.tse.fise2.pip.Exceptions;

import java.lang.Exception;

/**
 * Methode permettant de gerer le cas ou un utilisateur n'existe pas lors de la
 * recherche
 * 
 * @author Yammine Eric
 *
 */

@SuppressWarnings("serial")
public class UserNotExistException extends Exception {
	public UserNotExistException() {
		super();
	}
}