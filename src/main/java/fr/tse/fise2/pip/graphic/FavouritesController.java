package fr.tse.fise2.pip.graphic;

import fr.tse.fise2.pip.Exceptions.IncorrectSyntax;
import fr.tse.fise2.pip.Utils.Session;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

import java.util.ArrayList;

/**
 * Controlleur faisant le lien avec l'interface graphique des favoris.
 * 
 * @author Queffelec Matthieu
 */
public class FavouritesController {

	private MainController mainController;

	@FXML
	private ListView<String> listView;

	@FXML
	private Button searchButton;

	@FXML
	private Button delButton;

	public FavouritesController(MainController mainController) {
		this.mainController = mainController;
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur le bouton supprimer. Elle
	 * verifie qu'un item est bien selectionner puis supprime le favoris de
	 * l'affichage et de la BDD.
	 * 
	 * @param event
	 */
	@FXML
	void delButtonClick(ActionEvent event) {
		if (listView.getSelectionModel().getSelectedItem() != null) {
			Session session = mainController.getSession();
			String wordToRemove = listView.getSelectionModel().getSelectedItem();
			session.getDatabase().removeFavourites(wordToRemove, session);
			listView.getItems().clear();
			setScene(session.getDatabase().displayFavourites(session));
		} else {
			Alert alert = AlertCreator.createError("Vous n'avez pas selectionne de favoris !");
			alert.showAndWait();
		}
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur la recherche d'un favoris. Le
	 * comportement est le meme que pour une recherche standard.
	 * 
	 * @param event
	 * @throws IncorrectSyntax
	 */
	@FXML
	void searchClick(ActionEvent event) throws IncorrectSyntax {
		if (listView.getSelectionModel().getSelectedItem() != null) {
			String wordToSearch = listView.getSelectionModel().getSelectedItem();
			mainController.getSearchBar().setText(wordToSearch);
			mainController.searchSceneBuilder();
		} else {
			Alert alert = AlertCreator.createError("Vous n'avez pas selectionne de favoris !");
			alert.showAndWait();
		}
	}

	/**
	 * Methode qui met en place la liste des favoris dans la ListView.
	 * 
	 * @param favs la liste des favoris a afficher.
	 */
	public void setScene(ArrayList<String> favs) {
		ObservableList<String> obsFav = FXCollections.<String>observableArrayList(favs);
		listView.getItems().addAll(obsFav);
	}

}