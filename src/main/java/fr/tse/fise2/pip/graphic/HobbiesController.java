package fr.tse.fise2.pip.graphic;

import fr.tse.fise2.pip.Exceptions.IncorrectSyntax;
import fr.tse.fise2.pip.Utils.Session;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Controller qui gere les interactions de l'utilisateur avec l'affichage des
 * POI.
 *
 * @author Queffelec Matthieu
 */
public class HobbiesController {

	private MainController mainController;
	private ArrayList<ArrayList<String>> hobbies;
	private String hobbySelected = null;
	private String keywordSelected = null;
	private Stage secondStage;
	private Stage primaryStage;

	@FXML
	private ListView<String> hobbiesList;

	@FXML
	private ListView<String> keywordList;

	public HobbiesController(MainController mainController) {
		this.mainController = mainController;
		this.primaryStage = mainController.getStage();
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur le bouton d'ajout de POI.
	 * 
	 * @param event
	 */
	@FXML
	void addHobbyClick(ActionEvent event) {
		displayPopup("Hobby");
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur le bouton d'ajout d'un
	 * element dans un POI. Elle verifie d'abord qu'un POI est selectionne
	 * 
	 * @param event
	 */
	@FXML
	void addKeywordClick(ActionEvent event) {
		if (hobbySelected == null) {
			Alert alert = AlertCreator.createError("Aucun point d'interet n'est selectionne.");
			alert.showAndWait();
		} else {
			displayPopup("Keyword");
		}
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur le bouton de suppression de
	 * POI. La methode supprime le POI de la memoire et de l'affichage.
	 * 
	 * @param event
	 */
	@FXML
	void delHobbyClick(ActionEvent event) {
		if (hobbySelected == null) {
			Alert alert = AlertCreator.createError("Aucun point d'interet n'est selectionne.");
			alert.showAndWait();
		} else {
			ArrayList<String> hobby, hobbyToRemove = null;
			Iterator<ArrayList<String>> it = hobbies.iterator();
			while (it.hasNext()) {
				hobby = it.next();
				if (hobby.get(0).equals(hobbySelected)) {
					hobbyToRemove = hobby;
				}
			}
			hobbies.remove(hobbyToRemove);
			updateHobbies();
			hobbySelected = null;
			keywordList.getItems().clear();
			save();
		}
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur le bouton de suppression
	 * d'element d'un POI. La methode supprime l'element de la memoire et de
	 * l'affichage.
	 * 
	 * @param event
	 */
	@FXML
	void delKeywordClick(ActionEvent event) {
		if (keywordSelected == null) {
			Alert alert = AlertCreator.createError("Aucun element n'est selectionne.");
			alert.showAndWait();
		} else {
			ArrayList<String> hobby, hobbyToRemove = null;
			Iterator<ArrayList<String>> it = hobbies.iterator();
			while (it.hasNext()) {
				hobby = it.next();
				if (hobby.get(0).equals(hobbySelected)) {
					hobbyToRemove = hobby;
				}
			}
			String replace = hobbyToRemove.get(1);
			if (replace == keywordSelected) {
				replace = "";
			} else {
				int index = replace.indexOf(keywordSelected);
				if (index == 0) {
					replace = replace.substring(keywordSelected.length() + 1);
				} else if (index + keywordSelected.length() == replace.length()) {
					replace = replace.substring(0, index - 1);
				} else {
					replace = replace.substring(0, index - 1) + replace.substring(index + keywordSelected.length());
				}

			}
			hobbyToRemove.remove(1);
			hobbyToRemove.add(replace);
			updateKeyword();
			save();
		}
	}

	/**
	 * Methode qui sauvegarde les POI dans la base de donnee.
	 * 
	 * @param event
	 */
	@FXML
	void saveHobbiesClick(ActionEvent event) {
		save();
	}

	@FXML
	void viewStatClick(ActionEvent event) {
		if (hobbySelected == null) {
			Alert alert = AlertCreator.createError("Vous devez selectionner un point d'interet.");
			alert.showAndWait();
		} else if (isHobbyEmpty(hobbySelected)) {
			Alert alert = AlertCreator.createError("Ce point d'interet n'a pas d'element.");
			alert.showAndWait();
		} else {
			Iterator<String> it = keywordList.getItems().iterator();
			String hobby = it.next();
			while (it.hasNext()) {
				hobby = hobby + " " + it.next();
			}
			try {
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(UserStatsController.class.getResource("/fxml/DisplayHobbies.fxml"));
				DisplayHobbiesController displayHobbiesController = new DisplayHobbiesController(mainController);
				loader.setController(displayHobbiesController);
				AnchorPane layout = loader.load();
				Scene secondScene = new Scene(layout);
				displayHobbiesController.setScene(hobby);
				secondStage = new Stage();
				secondStage.setTitle("Statistique du point d'interet");
				secondStage.setScene(secondScene);

				// Specifies the modality for new window.
				secondStage.initModality(Modality.WINDOW_MODAL);

				// Specifies the owner Window (parent) for new window
				secondStage.initOwner(primaryStage);

				// Set position of second window, related to primary window.
				secondStage.setX(primaryStage.getX() + 200);
				secondStage.setY(primaryStage.getY() + 100);

				secondStage.show();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur la liste des POI et lance la
	 * mise a jour de l'affichage de element du POI.
	 * 
	 * @param event
	 */
	@FXML
	void mouseHobbyClicked(MouseEvent event) {
		if (hobbiesList.getSelectionModel().getSelectedItem() != null) {
			keywordSelected = null;
			hobbySelected = hobbiesList.getSelectionModel().getSelectedItem();
			updateKeyword();
		}
	}

	/**
	 * Methode qui gere le clique de l'utilisateur sur la liste des elements.
	 * 
	 * @param event
	 */
	@FXML
	void mouseKeywordClicked(MouseEvent event) {
		if (keywordList.getSelectionModel().getSelectedItem() != null) {
			keywordSelected = keywordList.getSelectionModel().getSelectedItem();
		}
	}

	/**
	 * Methode qui permet de mettre a jour l'affichage des POI.
	 * 
	 * @param hobbiesNew la liste des POI a afficher.
	 */
	public void setScene(ArrayList<ArrayList<String>> hobbiesNew) {
		this.hobbies = hobbiesNew;
		updateHobbies();
	}

	/**
	 * Methode qui efface l'affichage des elements des POI pour afficher la liste
	 * correspondante au POI selectionnee.
	 */
	public void updateKeyword() {
		keywordList.getItems().clear();
		Iterator<ArrayList<String>> it = hobbies.iterator();
		ArrayList<String> hobby = null;
		ArrayList<String> keywordsToDisplay = null;
		while (it.hasNext()) {
			hobby = it.next();
			if (hobby.get(0).equals(hobbySelected)) {
				keywordsToDisplay = hobby;
			}
		}
		if (keywordsToDisplay.size() > 1) {
			ArrayList<String> keywords = new ArrayList<String>(Arrays.asList(keywordsToDisplay.get(1).split(";")));
			ObservableList<String> obsKeyword = FXCollections.<String>observableArrayList(keywords);
			keywordList.getItems().addAll(obsKeyword);
		}

	}

	/**
	 * Methode qui affiche un popup pour permettre a l'utilisateur de rentrer un
	 * nouveau POI ou element d'un POI.
	 * 
	 * @param type String indiquant si il faut que le popup soit celui des POI ou
	 *             des elements.
	 */
	public void displayPopup(String type) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(UserStatsController.class.getResource("/fxml/HobbiePopup.fxml"));
			HobbiePopupController popupController = new HobbiePopupController(this, type);
			loader.setController(popupController);
			AnchorPane layout = loader.load();
			Scene secondScene = new Scene(layout);
			popupController.setScene();
			secondStage = new Stage();
			secondStage.setTitle("New " + type);
			secondStage.setScene(secondScene);

			// Specifies the modality for new window.
			secondStage.initModality(Modality.WINDOW_MODAL);

			// Specifies the owner Window (parent) for new window
			secondStage.initOwner(primaryStage);

			// Set position of second window, related to primary window.
			secondStage.setX(primaryStage.getX() + 200);
			secondStage.setY(primaryStage.getY() + 100);

			secondStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Methode qui permet d'ajouter un element au POI selectionne. Il met a jour
	 * l'affichage et la liste de hobby en memoire.
	 * 
	 * @param keyword l'element a ajouter.
	 */
	public void addKeyword(String keyword) {
		secondStage.close();
		keywordList.getItems().add(keyword);
		ArrayList<String> keywordListEdited = null;
		Iterator<ArrayList<String>> it = hobbies.iterator();
		while (it.hasNext()) {
			ArrayList<String> hobby = it.next();
			if (hobby.get(0).equals(hobbySelected)) {
				keywordListEdited = hobby;
			}
		}
		if (keywordListEdited.size() == 1) {
			keywordListEdited.add(keyword);
		} else {
			String replace = keywordListEdited.get(1) + ";" + keyword;
			keywordListEdited.remove(1);
			keywordListEdited.add(replace);
		}
		save();
	}

	/**
	 * Methode qui permet d'ajouter un POI. Met a jour l'affichage et la liste de
	 * POI en memoire.
	 * 
	 * @param hobby String contenant le nom du nouveau POI.
	 */
	public void addHobby(String hobby) {
		secondStage.close();
		ArrayList<String> newHobby = new ArrayList<String>();
		newHobby.add(hobby);
		hobbies.add(newHobby);
		hobbiesList.getItems().add(hobby);
		save();
	}

	/**
	 * Methode qui met a jour l'affichage des POI.
	 */
	public void updateHobbies() {
		hobbiesList.getItems().clear();
		ArrayList<String> hobbiesName = new ArrayList<String>();
		Iterator<ArrayList<String>> it = hobbies.iterator();
		while (it.hasNext()) {
			hobbiesName.add(it.next().get(0));
		}
		ObservableList<String> obsHobbies = FXCollections.<String>observableArrayList(hobbiesName);
		hobbiesList.getItems().addAll(obsHobbies);
	}

	/**
	 * Methode qui sauvegarde les point d'interet dans la BDD
	 */
	public void save() {
		Session session = mainController.getSession();
		session.getDatabase().removeAllHobbies(session);
		ArrayList<String> hobby;
		Iterator<ArrayList<String>> it = hobbies.iterator();
		while (it.hasNext()) {
			hobby = it.next();
			if (hobby.size() == 2) {
				try {
					session.getDatabase().addHobbies(hobby.get(0), hobby.get(1), session);
				} catch (IncorrectSyntax incorrectSyntax) {
					incorrectSyntax.printStackTrace();
					Alert alert = AlertCreator.createError("La syntaxe est incorrecte");
					alert.showAndWait();
				}
			} else {
				try {
					session.getDatabase().addHobbies(hobby.get(0), "", session);
				} catch (IncorrectSyntax incorrectSyntax) {
					incorrectSyntax.printStackTrace();
					Alert alert = AlertCreator.createError("La syntaxe est incorrecte");
					alert.showAndWait();
				}
			}
		}
	}

	/**
	 * Verifie si le POI specifie n'a pas d'elements
	 * 
	 * @param hobbyToCheck POI a verifier
	 * @return boolean qui indique si le POI est vide ou non
	 */
	public boolean isHobbyEmpty(String hobbyToCheck) {
		ArrayList<String> hobby;
		Iterator<ArrayList<String>> it = hobbies.iterator();
		while (it.hasNext()) {
			hobby = it.next();
			if (hobby.get(0).equals(hobbyToCheck) && hobby.get(1).equals("")) {
				return true;
			}
		}
		return false;
	}
}