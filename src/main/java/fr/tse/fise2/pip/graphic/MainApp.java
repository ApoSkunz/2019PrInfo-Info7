package fr.tse.fise2.pip.graphic;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Classe principale du programme. Elle cree l'interface graphique et le
 * controlleur principal.
 * 
 * @author Saunier Malo, Queffelec Matthieu
 */
public class MainApp extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;
	private MainController mainController;

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Twitter Analytics");
		this.primaryStage.getIcons().add(new Image("file:images/favicon.png"));
		Image faviconEclipse = new Image("file:src/main/resources/images/favicon.png");
		Image faviconDesktopApp = new Image("/images/favicon.png");
		if (!faviconDesktopApp.isError()) {
			this.primaryStage.getIcons().add(faviconDesktopApp);
		} else {
			this.primaryStage.getIcons().add(faviconEclipse);
		}
		primaryStage.setMinHeight(550);
		primaryStage.setMinWidth(750);
		initRootLayout();

	}

	/**
	 * Methode qui initialise la fenetre.
	 */
	private void initRootLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("/fxml/RootLayout.fxml"));
			mainController = new MainController(primaryStage);
			loader.setController(mainController);
			rootLayout = (BorderPane) loader.load();
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);

			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public static void main(String[] args) {
		launch(args);
	}

}