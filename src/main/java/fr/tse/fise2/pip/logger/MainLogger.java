package fr.tse.fise2.pip.logger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainLogger {
	private static Logger logger = LogManager.getLogger(MainLogger.class.getName());

	public static void main(String[] args) {
		logger.debug("msg de debogage");
		logger.info("msg d'information");
		logger.warn("msg d'avertissement");
		logger.error("msg d'erreur");
		logger.fatal("msg d'erreur fatale");
	}
}
