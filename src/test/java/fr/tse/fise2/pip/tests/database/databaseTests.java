package fr.tse.fise2.pip.tests.database;

import fr.tse.fise2.pip.Exceptions.IncorrectPasswordException;
import fr.tse.fise2.pip.Exceptions.IncorrectSpaceInUsername;
import fr.tse.fise2.pip.graphic.MainController;
import javafx.stage.Stage;
import org.junit.Test;

import fr.tse.fise2.pip.Utils.Session;
import fr.tse.fise2.pip.Database.Database;
import fr.tse.fise2.pip.Exceptions.UserAlreadyRegisteredException;
import fr.tse.fise2.pip.Exceptions.UserNotExistException;

import static org.junit.Assert.assertEquals;

public class databaseTests {
	@Test(expected = UserAlreadyRegisteredException.class)
	/**
	 * Methode de test permettant d'ajouter deux utilisateurs dans la table Accounts
	 * avec le meme nom afin de lever l'exception
	 * 
	 * @throws UserAlreadyRegisteredException renvoie une exception si un
	 *                                        utilisateur avec le meme nom existe
	 */
	public void insertTest() throws UserAlreadyRegisteredException, IncorrectSpaceInUsername {
		Database db = new Database();
		db.registerAccount("test4", "testmdp2", "test");
		db.registerAccount("test4", "testmdp", "test");
	}

	@Test
	/**
	 * Methode permettant de tester l'inscription de compte avec connexion a
	 * celui-ci
	 */
	public void registerTest() throws IncorrectSpaceInUsername {
		try {
			Database db = new Database();
			Stage primaryStage = new Stage();
			Session session = new Session(new MainController(primaryStage));
			db.registerAccount("test", "testmdp2", "test");
			db.loginAccount("test", "testmdp2", session);
			assertEquals("test", session.getUsername());
		} catch (UserAlreadyRegisteredException e) {
			e.printStackTrace();
		} catch (UserNotExistException e) {
			e.printStackTrace();
		} catch (IncorrectPasswordException e) {
			e.printStackTrace();
		}

	}

	@Test
	/**
	 * Methode permettant de tester la connection a un compte avec un mot de passe
	 * faux, un nom d'utilisateur qui n'existe pas et enfin la connexion avec le bon
	 * nom d'utilisateur et le bon mot de passe afin de tester toutes les exceptions
	 * 
	 * @throws UserAlreadyRegisteredException renvoie une exception si un
	 *                                        utilisateur avec le meme existe deja
	 *                                        donc inscription impossible
	 * @throws UserNotExistException          renvoie une exception si un
	 *                                        utilisateur avec le nom d'utilisateur
	 *                                        saisi n'existe pas
	 * @throws IncorrectPasswordException     renvoie une exception si le mot passe
	 *                                        saisi n'est pas le bon pour le nom
	 *                                        d'utilisateur saisi existant
	 */
	public void loginTest() throws UserAlreadyRegisteredException, UserNotExistException, IncorrectPasswordException,
			IncorrectSpaceInUsername {
		Database db = new Database();
		db.registerAccount("testAccount", "testPass", "test");
		Stage primaryStage = new Stage();
		Session session = new Session(new MainController(primaryStage));
		boolean connect = db.loginAccount("testAccount", "fauxmdp", session);
		assertEquals(false, connect);
		connect = db.loginAccount("existepas", "mdp", session);
		assertEquals(false, connect);
		connect = db.loginAccount("testAccount", "testPass", session);
		assertEquals(true, connect);
	}

}
