package fr.tse.fise2.pip.Exceptions;

/**
 * Exception permettant de gerer le cas ou l'utilisateur saisi un nom
 * du'tilisateur contenant un espace
 * 
 * @author Yammine Eric
 *
 */
@SuppressWarnings("serial")
public class IncorrectSpaceInUsername extends Exception {
	public IncorrectSpaceInUsername() {
		super();
	}

}
