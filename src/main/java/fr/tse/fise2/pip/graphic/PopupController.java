package fr.tse.fise2.pip.graphic;

import fr.tse.fise2.pip.Exceptions.IncorrectSyntax;
import fr.tse.fise2.pip.Exceptions.UserNotExistException;
import fr.tse.fise2.pip.Utils.SearchUtils;
import fr.tse.fise2.pip.Utils.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

/**
 * Controller faisant le lien avec l'interface graphique qui affiche le popup
 * pour entrer le compte avec lequel effectuer la comparaison.
 * 
 * @author Queffelec Matthieu
 */
public class PopupController {

	private UserStatsController userStatsController;
	@FXML
	private TextField textField;
	@FXML
	private Button compareButton;

	public PopupController(UserStatsController userStatsController) {
		this.userStatsController = userStatsController;
	}

	@FXML
	void compareClick(ActionEvent event) throws IncorrectSyntax {
		compare();
	}

	@FXML
	void keyPressed(KeyEvent event) throws IncorrectSyntax {
		if (event.getCode().toString().equals("ENTER")) {
			compare();
		}
	}

	/**
	 * Methode qui teste que l'utilisateur a bien rentrer du texte et que celui-ci
	 * correspond bien a un compte. Sinon souleve une alerte.
	 * 
	 * @throws IncorrectSyntax
	 */
	public void compare() throws IncorrectSyntax {
		String secoundUser = textField.getText();
		if (secoundUser.equals("")) {
			Alert alert = AlertCreator.createError("Vous n'avez pas rentre de compte.");
			alert.showAndWait();
		} else {
			String identifier = secoundUser.substring(0, 1);
			String searchWord = secoundUser.substring(1);
			if (!identifier.equals("@")) {
				Alert alert = AlertCreator.createError("Les recherches de compte doivent commencer par '@'.");
				alert.showAndWait();
			} else {
				try {
					User user2 = SearchUtils.makeSearch(searchWord);
					userStatsController.compareAccounts(user2);
				} catch (UserNotExistException e) {
					e.printStackTrace();
					Alert alert = AlertCreator.createError("Le compte recherche n'existe pas.");
					alert.showAndWait();
				}
			}
		}
	}
}
