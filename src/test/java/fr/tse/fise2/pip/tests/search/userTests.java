package fr.tse.fise2.pip.tests.search;

import java.io.IOException;
import java.util.SortedSet;

import org.junit.Assert;
import org.junit.Test;
import fr.tse.fise2.pip.Utils.Hashtag;

import fr.tse.fise2.pip.Utils.SearchUtils;
import fr.tse.fise2.pip.Utils.User;
import fr.tse.fise2.pip.Exceptions.IncorrectSyntax;
import fr.tse.fise2.pip.Exceptions.PrivateUserException;
import fr.tse.fise2.pip.Exceptions.UserNotExistException;

public class userTests {

	@Test
	/**
	 * Test unitaire pour verifier si l'utilisateur projet info 4 existe, et pour
	 * verifier si l'egalite de 2 User est bien base sur le screen name
	 */
	public void user_test() throws IncorrectSyntax {
		User user = null;
		try {
			user = SearchUtils.makeSearch("projetinfo4");
		} catch (UserNotExistException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		User user2 = new User();
		user2.setScreen_name("projetinfo4");
		Assert.assertEquals(user, user2);
	}

	@Test
	/**
	 * Test unitaire pour verifier si la fonction permettant de recuperer les
	 * hashtags les plus utilises par un utiisateur sont bien recuperes
	 */
	public void mainHashtagsFromUser_test()
			throws IOException, InterruptedException, PrivateUserException, IncorrectSyntax {
		User user = null;
		try {
			user = SearchUtils.makeSearch("realdonaldtrump");
		} catch (UserNotExistException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SortedSet<Hashtag> mainHashtagsUsed = SearchUtils.getMainHashtagsFrom(user);
		Assert.assertTrue(mainHashtagsUsed.size() > 0);
	}
}
