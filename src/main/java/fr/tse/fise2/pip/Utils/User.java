package fr.tse.fise2.pip.Utils;

import com.google.gson.JsonObject;
import fr.tse.fise2.pip.Exceptions.UserNotExistException;

/**
 * POJO User permettant de creer et "stocker" les donnees d'un utilisateur
 * Twitter
 * 
 * @author Yammine Eric et Solane Alexandre
 *
 */
public class User {
	private String name;
	private String screen_name;
	private Integer followers_count;
	private Integer friends_count;
	private String profile_image_url;
	private String profile_banner_url = "https://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png";
	private Integer statuses_count;

	/**
	 * Constructeur de la classe avec un Json passe en parametre
	 * 
	 * @param json Objet Json contenant les informations de l'utilisateur Twitter
	 *             (recupere par une requette HTTP en general)
	 * @throws UserNotExistException Renvoie une exception si l'utilisateur specifie
	 *                               par le Json n'existe pas
	 */
	public User(JsonObject json) throws UserNotExistException {
		try {
			this.name = json.get("name").getAsString();
			this.screen_name = json.get("screen_name").getAsString();
			this.followers_count = json.get("followers_count").getAsInt();
			this.friends_count = json.get("friends_count").getAsInt();
			this.profile_image_url = json.get("profile_image_url").getAsString();

			if (json.get("profile_banner_url") != null) {
				this.profile_banner_url = json.get("profile_banner_url").getAsString();
			}
			this.statuses_count = json.get("statuses_count").getAsInt();
		} catch (NullPointerException e) {
			throw new UserNotExistException();
		}

	}

	/**
	 * Constructeur de la classe avec les donnees rentrees manuellement
	 * 
	 * @param name               nom sur twitter
	 * @param screen_name        d'utilisateur sur twitter
	 * @param followers_count    nombre de followers
	 * @param friends_count      nombre d'abonnements
	 * @param profile_image_url  photo de profil de l'utilisateur
	 * @param profile_banner_url banniere twitter
	 * @param statuses_count     nombre de tweets et retweets de l'utilisateur
	 */
	public User(String name, String screen_name, Integer followers_count, Integer friends_count,
			String profile_image_url, String profile_banner_url, Integer statuses_count) {
		super();
		this.name = name;
		this.screen_name = screen_name;
		this.followers_count = followers_count;
		this.friends_count = friends_count;
		this.profile_image_url = profile_image_url;
		this.profile_banner_url = profile_banner_url;
		this.statuses_count = statuses_count;
	}

	public User() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getScreen_name() {
		return screen_name;
	}

	public void setScreen_name(String screen_name) {
		this.screen_name = screen_name;
	}

	public Integer getFollowers_count() {
		return followers_count;
	}

	public void setFollowers_count(int followers_count) {
		this.followers_count = followers_count;
	}

	public Integer getFriends_count() {
		return friends_count;
	}

	public void setFriends_count(int friends_count) {
		this.friends_count = friends_count;
	}

	public String getProfile_image_url() {
		return profile_image_url;
	}

	public void setProfile_image_url(String profile_image_url) {
		this.profile_image_url = profile_image_url;
	}

	public String getProfile_banner_url() {
		return profile_banner_url;
	}

	public void setProfile_banner_url(String profile_banner_url) {
		this.profile_banner_url = profile_banner_url;
	}

	public Integer getStatuses_count() {
		return statuses_count;
	}

	public void setStatuses_count(Integer statuses_count) {
		this.statuses_count = statuses_count;
	}

	public void setFollowers_count(Integer followers_count) {
		this.followers_count = followers_count;
	}

	public void setFriends_count(Integer friends_count) {
		this.friends_count = friends_count;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((screen_name == null) ? 0 : screen_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (screen_name == null) {
			if (other.screen_name != null)
				return false;
		} else if (!screen_name.equals(other.screen_name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", screen_name=" + screen_name + ", followers_count=" + followers_count
				+ ", friends_count=" + friends_count + ", profile_image_url=" + profile_image_url
				+ ", profile_background_image_url=" + profile_banner_url + ", statuses_count=" + statuses_count + "]";
	}

}
