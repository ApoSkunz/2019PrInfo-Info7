package fr.tse.fise2.pip.graphic;

import fr.tse.fise2.pip.Exceptions.IncorrectSyntax;
import fr.tse.fise2.pip.Exceptions.UserNotExistException;
import fr.tse.fise2.pip.Utils.SearchUtils;
import fr.tse.fise2.pip.Utils.Utils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

/**
 * Controller qui fait le lien avec l'affichage du popup pour l'ajout d'un POI
 * ou d'un element.
 * 
 * @author Queffelec Matthieu
 */
public class HobbiePopupController {

	private HobbiesController hobbiesController;
	private String type;
	@FXML
	private TextField textField;
	@FXML
	private Button compareButton;
	@FXML
	private Label label;

	public HobbiePopupController(HobbiesController hobbiesController, String type) {
		this.hobbiesController = hobbiesController;
		this.type = type;
	}

	@FXML
	void compareClick(ActionEvent event) throws IncorrectSyntax {
		add();
	}

	@FXML
	void keyPressed(KeyEvent event) throws IncorrectSyntax {
		if (event.getCode().toString().equals("ENTER")) {
			add();
		}
	}

	/**
	 * Methode qui met en place le popup en fonction du type de l'ajout, un POI ou
	 * un element.
	 */
	public void setScene() {
		if (type.equals("Keyword")) {
			label.setText("Entrez la recherche a ajouter au point d'interet : ");
		} else {
			label.setText("Entrez le nom du nouveau point d'interet : ");
		}

	}

	/**
	 * Methode qui verifie que le mot rentrer est bien ecrit et qui appelle la bonne
	 * fonction dans le controlleur de POI.
	 * 
	 * @throws IncorrectSyntax
	 */
	public void add() throws IncorrectSyntax {
		String word = textField.getText();
		String resultConcatPregMatch = Utils.resultConcatPregMatch(word, "([#@]{1}[@# ]{1,})|([#@]{1}$)|(^[ ]{1,}$)");
		if (word.equals("")) {
			Alert alert = AlertCreator.createError("Entrez un mot a ajouter.");
			alert.showAndWait();
		} else if (resultConcatPregMatch.equals("")) {
			if (type.equals("Keyword")) {
				if (word.contains(";")) {
					Alert alert = AlertCreator.createError("Le mot ne peut pas contenir de ';'.");
					alert.showAndWait();
				} else {
					if (word.substring(0, 1).equals("@")) {
						String user = word.substring(1);
						try {
							SearchUtils.makeSearch(user);
							hobbiesController.addKeyword(word);
						} catch (UserNotExistException e) {
							Alert alert = AlertCreator.createError("L'utilisateur que vous avez rentre n'existe pas.");
							alert.showAndWait();
						}
					} else {
						hobbiesController.addKeyword(word);
					}
				}
			} else {
				hobbiesController.addHobby(word);
			}

		} else {
			Alert alert = AlertCreator.createError(
					"Un point d'interet ne peut pas avoir un utilisateur vide, un hashtag vide ou un mot clef vide");
			alert.showAndWait();
		}
	}
}
