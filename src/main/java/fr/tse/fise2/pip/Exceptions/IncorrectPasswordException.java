package fr.tse.fise2.pip.Exceptions;

/**
 * Exception permettant de gerer le cas ou l'utilisateur saisi un mauvais mot de
 * passe lors de la connexion
 * 
 * @author Yammine Eric
 *
 */
@SuppressWarnings("serial")
public class IncorrectPasswordException extends Exception {
	public IncorrectPasswordException() {
		super();
	}
}