package fr.tse.fise2.pip.Utils;

import java.io.IOException;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonArray;

import fr.tse.fise2.pip.Exceptions.IncorrectSyntax;
import fr.tse.fise2.pip.Exceptions.PrivateUserException;
import fr.tse.fise2.pip.Exceptions.UserNotExistException;
import fr.tse.fise2.pip.logger.MainLogger;

/**
 * Classe gerant les interactions de recherche entre l'interface utilisateur et
 * l'API
 * 
 * @author Yammine Eric, Solane Alexandre
 *
 */
public class SearchUtils {
	static Logger logger = LogManager.getLogger(MainLogger.class.getName());

	/**
	 * Methode permettant la recherche d'un utilisateur grace a son screen name
	 * 
	 * @param toBeFound String contenant le nom de l'utilisateur a rechercher
	 * @throws UserNotExistException Renvoie une exception si l'utilisateur
	 *                               recherche n'existe pas
	 * @return userToFind renvoie un objet User representant l'utilsiateur qui a ete
	 *         recherche
	 * @throws IncorrectSyntax
	 */

	public static User makeSearch(String toBeFound) throws UserNotExistException, IncorrectSyntax {
		User userToFind = null;
		String resultConcatPregMatch = Utils.resultConcatPregMatch(toBeFound,
				"([#@]{1}[@# ]{1,})|([#@]{1}$)|(^[ ]{1,}$)");
		if (resultConcatPregMatch.equals("")) {
			try {
				userToFind = new User(APIUtils.search(toBeFound));
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			logger.warn("Un utilisateur commence par @ et se continue par au moins un chiffre ou une lettre");
			throw new IncorrectSyntax();
		}

		return userToFind;
	}

	/**
	 * Methode permettant de recuperer les hashtags les plus utilises pour un
	 * utilisateur
	 * 
	 * @param user Utilisateur sur lequel on va effectuer la recherche
	 * @return Set trie par ordre decroissant des hashtags les plus utilises par
	 *         l'utilisateur passe en parametres
	 * @throws IOException          exception
	 * @throws InterruptedException exception
	 * @throws PrivateUserException exception
	 */
	public static SortedSet<Hashtag> getMainHashtagsFrom(User user)
			throws IOException, InterruptedException, PrivateUserException {

		try {
			JsonArray tweetList = APIUtils.getLastTweetsFrom(user);
			ArrayList<Hashtag> hashtagsUsed = JsonUtils.getHashtagsFromTweetList(tweetList, user.getName());

			SortedSet<Hashtag> mainHashtags = new TreeSet<Hashtag>();
			for (Hashtag hashtag : hashtagsUsed) {
				if (!mainHashtags.contains(hashtag)) {
					hashtag.setCount((double) Collections.frequency(hashtagsUsed, hashtag));
					mainHashtags.add(hashtag);
				}
			}
			return mainHashtags;
		} catch (IllegalStateException e) {
			throw new PrivateUserException();
		}
	}

	/**
	 * Methode permettant de recuperer le top des hashtags associes a un centre
	 * d'interet ou un autre hashtag
	 * 
	 * @param wordOrHashtag Le centre d'interet ou le hashtag sur lequel on va faire
	 *                      la recherche
	 * @return Set trie par ordre decroissant du "top" des hashtags associes a la
	 *         recherche
	 * @throws IOException          exception
	 * @throws InterruptedException exception
	 * @throws IncorrectSyntax
	 */
	public static SortedSet<Hashtag> getMainHashtagsFrom(String wordOrHashtag)
			throws IOException, InterruptedException, IncorrectSyntax {
		SortedSet<Hashtag> mainHashtags = new TreeSet<Hashtag>();
		String resultConcatPregMatch = Utils.resultConcatPregMatch(wordOrHashtag,
				"([#@]{1}[@# ]{1,})|([#@]{1}$)|(^[ ]{1,}$)");
		if (resultConcatPregMatch.equals("")) {
			JsonArray tweetList = APIUtils.getLastTweetsFrom(wordOrHashtag);
			ArrayList<Hashtag> hashtagsUsed = JsonUtils.getHashtagsFromTweetList(tweetList, wordOrHashtag);

			for (Hashtag hashtag : hashtagsUsed) {
				if (!mainHashtags.contains(hashtag)) {
					hashtag.setCount((double) Collections.frequency(hashtagsUsed, hashtag));
					mainHashtags.add(hashtag);
				}

			}
		} else {
			logger.warn("Une recherche ne peut pas avoir un utilisateur vide, un hashtag vide ou un mot clef vide");
			throw new IncorrectSyntax();
		}
		return mainHashtags;
	}

	/**
	 * Methode permettant de recuperer le top des tweets associes a un point
	 * d'interet
	 * 
	 * @param hobbies elements associes au point d'interet sous forme de string
	 *                '#hastag1 keyword2 @account3'
	 * @return le top des tweets tries selon la popularite
	 * @throws IOException          exception
	 * @throws InterruptedException exception
	 */
	public static SortedSet<Tweet> getMainTweetsFrom(String hobbies) throws IOException, InterruptedException {
		JsonArray tweetList = APIUtils.getLastTweetsFrom(hobbies + " exclude:retweets");
		ArrayList<Tweet> tweetOnHobbies = JsonUtils.getTweetsFromTweetList(tweetList);
		SortedSet<Tweet> mainTweets = new TreeSet<Tweet>();
		for (Tweet tweet : tweetOnHobbies) {
			mainTweets.add(tweet);
		}
		return mainTweets;
	}

	/**
	 * Methode permettant d'obtenir les hashtags(excluant les RT) les plus utilises
	 * pour la recherche de hobbies
	 * 
	 * @param hobbies elements associes au point d'interet sous forme de string
	 *                '#hastag1 keyword2 @account3'
	 * @return les hashtags les plus utilises
	 * @throws IOException          exception
	 * @throws InterruptedException exception
	 */
	public static SortedSet<Hashtag> getRecurrentHashtagsFrom(String hobbies) throws IOException, InterruptedException {
		JsonArray tweetList = APIUtils.getLastTweetsFrom(hobbies + " exclude:retweets");
		ArrayList<Hashtag> hashtagsUsed = JsonUtils.getHashtagsFromTweetList(tweetList, hobbies);

		SortedSet<Hashtag> mainHashtags = new TreeSet<Hashtag>();
		for (Hashtag hashtag : hashtagsUsed) {
			if (!mainHashtags.contains(hashtag)) {
				hashtag.setCount((double) Collections.frequency(hashtagsUsed, hashtag));
				mainHashtags.add(hashtag);
			}

		}
		return mainHashtags;
	}

	/**
	 * Methode permettant de recuperer le nombre d'utilisateurs uniques tweetant sur
	 * un hashtag ou un centre d'interet
	 * 
	 * @param wordOrHashtag Le centre d'interet ou le hashtag sur lequel on va faire
	 *                      la recherche
	 * @return Le nombre d'utilisateurs tweetant sur le centre d'interet ou le
	 *         hashtag
	 * @throws IOException          exception
	 * @throws InterruptedException exception
	 * @throws IncorrectSyntax
	 */
	public static int getUniqueUserForHashtag(String wordOrHashtag)
			throws IOException, InterruptedException, IncorrectSyntax {
		String resultConcatPregMatch = Utils.resultConcatPregMatch(wordOrHashtag,
				"([#@]{1}[@# ]{1,})|([#@]{1}$)|(^[ ]{1,}$)");
		if (resultConcatPregMatch.equals("")) {
			JsonArray tweetList = APIUtils.getLastTweetsFrom(wordOrHashtag);
			Set<String> uniqueUserForHashtag = JsonUtils.getUniqueUserFromTweetList(tweetList);
			return uniqueUserForHashtag.size();
		} else {
			logger.warn("Une recherche ne peut pas avoir un utilisateur vide, un hashtag vide ou un mot clef vide");
			throw new IncorrectSyntax();
		}
	}

}
